# facbook

this is the demo project repo showing the project struct for REST API project 
in Golang.

the project might not work for you because it is just a demo to show you how I got REST API in Golang 


MIGRATIONS:
    there are migration files in directory ./migrations
    applying migrations, use goose (https://github.com/pressly/goose)
    run the following command from directory ./migrations
        'goose postgres "host=db_address port=db_port user=db_user password=db_password dbname=db_name sslmode=disable" up'

START PROJECT:
    run the following command to start the server 
        'go run main.go'
        
SWAGGER:
    find the swagger open api file in directory ./client/specs/