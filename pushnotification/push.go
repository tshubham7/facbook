package pushnotification

import (
	"log"
	"os"
	"strings"

	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/sideshow/apns2"
	"github.com/sideshow/apns2/payload"
	"github.com/sideshow/apns2/token"
)

var client *apns2.Client

func init() {

	key := os.Getenv("FACBOOK_PUSH_AUTH_KEY")
	key = strings.Replace(key, `\n`, "\n", 5)

	if key != "" {
		authKey, err := token.AuthKeyFromBytes([]byte(key))
		if err != nil {
			log.Fatal("token error:", err)
		}

		token := &token.Token{
			AuthKey: authKey,
			// KeyID from developer account (Certificates, Identifiers & Profiles -> Keys)
			KeyID: "4FM9SA5XPB",
			// TeamID from developer account (View Account -> Membership)
			TeamID: "PZNNEZ9VQZ",
		}
		client = apns2.NewTokenClient(token)
	} else {
		authKey, err := token.AuthKeyFromFile("./pushnotification/AuthKey_4FM9SA5XPB.p8")
		if err != nil {
			log.Fatal("token error:", err)
		}

		token := &token.Token{
			AuthKey: authKey,
			// KeyID from developer account (Certificates, Identifiers & Profiles -> Keys)
			KeyID: "4FM9SA5XPB",
			// TeamID from developer account (View Account -> Membership)
			TeamID: "PZNNEZ9VQZ",
		}
		client = apns2.NewTokenClient(token)
	}

}

// PushToUser push notification using user id
// all token will be fetched for these user
func PushToUser(userID, body string) {
	notification := &apns2.Notification{}
	notification.Topic = "io.facbook.in"

	payload := payload.NewPayload().Alert("push").AlertBody(body).Custom("id", "some group id")
	notification.Payload = payload
	notification.Priority = apns2.PriorityHigh
	tokens := getDeviceTokens(userID)
	for i := range tokens {
		notification.DeviceToken = tokens[i].Token
		_, err := client.Push(notification)
		if err != nil {
			log.Fatal("Error:", err)
		}
	}
}

// DeviceToken struct for select query
type DeviceToken struct {
	Token string `json:"token" db:"token"`
}

// returns the list of user device tokens
func getDeviceTokens(userID string) []DeviceToken {
	var tokens = []DeviceToken{}
	q := `SELECT token FROM user_devicetoken WHERE user_id=$1 AND is_active=true`
	err := db.DBWrite.Select(&tokens, q, userID)
	if err != nil {
		log.Println("go routine error: error while fetching device token for user_id: ", userID)
	}
	return tokens
}
