-- +goose Up
CREATE TABLE blog_report(
    id SERIAL PRIMARY KEY NOT NULL,
    is_checked BOOLEAN DEFAULT False,
    reporter_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES facbook_user(id),
    blog_id INTEGER REFERENCES blog(id) ON DELETE CASCADE,
    message TEXT DEFAULT '',
    type TEXT DEFAULT '',
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);

-- +goose Down
DROP TABLE blog_report;

