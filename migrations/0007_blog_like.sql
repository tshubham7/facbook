-- +goose Up
CREATE TABLE blog_like(
    blog_id INTEGER REFERENCES blog(id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE,
    timestamp timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX blog_like_user_id_fk ON blog_like(user_id);
CREATE INDEX blog_like_blog_id_fk ON blog_like(blog_id);


-- +goose Down

DROP INDEX blog_like_blog_id_fk;
DROP INDEX blog_like_user_id_fk;
DROP TABLE blog_like;
