-- +goose Up
CREATE TABLE blog(
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE,
    caption TEXT DEFAULT '',
    is_active BOOLEAN DEFAULT true,
    scheduled_time  timestamp with time zone DEFAULT NUll,
    media_key TEXT DEFAULT '',
    
    loc_name TEXT DEFAULT '',
    longitude double precision DEFAULT 0,
    latitude double precision DEFAULT 0,
    loc_position geometry(Point, 4326) DEFAULT NULL,
    deleted_at timestamp with time zone DEFAULT Null,  -- soft delete
    
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX blog_user_id_fk ON blog(user_id);

-- creating a view so that we won't have to add 'WHERE CLAUSE' everywhere in the project
CREATE OR REPLACE VIEW blog_view AS
SELECT * FROM blog WHERE deleted_at IS NULL AND is_active=true ORDER BY created_at DESC;

-- +goose Down
DROP VIEW blog_view;
DROP INDEX blog_user_id_fk;
DROP TABLE blog;
