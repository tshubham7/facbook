-- +goose Up
CREATE TABLE comment_like(
    comment_id INTEGER REFERENCES comment(id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE
);

CREATE INDEX comment_like_comment_id_fk ON comment_like(comment_id);
CREATE UNIQUE INDEX comment_like_comment_id_user_id_unique_fk ON comment_like(comment_id, user_id);


-- +goose Down

DROP INDEX comment_like_comment_id_fk;
-- DROP INDEX comment_like_comment_id_user_id_unique_fk;
DROP TABLE comment_like;
