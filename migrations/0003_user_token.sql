-- +goose Up

    
CREATE TABLE user_token(
    user_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE,
    mobile_otp text NOT NULL DEFAULT '',
    mobile_otp_expiry_time timestamp with time zone DEFAULT NULL,  --CURRENT_TIMESTAMP + INTERVAL '15 min'
    email_otp text NOT NULL DEFAULT '',
    email_otp_expiry_time timestamp with time zone DEFAULT NULL,  --CURRENT_TIMESTAMP + INTERVAL '15 min'
);


CREATE INDEX user_token_user_id_fk ON user_token(user_id);


-- +goose Down
DROP INDEX user_token_user_id_fk;
DROP TABLE user_token;



