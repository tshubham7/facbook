-- +goose Up
CREATE TABLE follow(
    follower_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE,
    following_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE,
    follow_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    accepted boolean DEFAULT false
);
CREATE INDEX follow_follower_id_fk ON follow(follower_id);
CREATE INDEX follow_following_id_fk ON follow(following_id);



-- +goose Down
DROP INDEX follow_follower_id_fk;
DROP INDEX follow_following_id_fk;

DROP TABLE follow;



