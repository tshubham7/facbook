-- +goose Up
CREATE TABLE user_report(
    id SERIAL PRIMARY KEY NOT NULL,
    type TEXT DEFAULT '',
    is_checked BOOLEAN DEFAULT False,
    reporter_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE,
    message TEXT DEFAULT '',
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


-- +goose Down
DROP TABLE user_report;


