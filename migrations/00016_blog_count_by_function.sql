-- +goose Up
-- -- get the count of blogs for the location
-- create or replace function blog_count_by_location(loc character)
--     returns integer as $$
--     declare
--     total integer;
--     begin
--     select Count(*) into total from blog where loc_name=loc;
--     return total;
--     end; $$
-- language plpgsql;

-- -- get the count of blogs for the hashtag
-- create or replace function blog_count_by_hashtag(hTag character)
--     returns integer as $$
--     declare
--     total integer;
--     begin
--     select count(id) into total from blog where caption like '%' || hTag ||'%';
--     return total;
--     end; $$ 
-- language plpgsql;

-- +goose Down



-- -- IN USE