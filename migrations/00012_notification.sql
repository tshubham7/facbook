-- +goose Up
CREATE TABLE notification(
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE,
    unread BOOLEAN DEFAULT TRUE,
    actor_object_id INTEGER DEFAULT 0,
    verb TEXT DEFAULT '',
    description TEXT DEFAULT '',
    extra_content TEXT DEFAULT '',
    -- target_object_id INTEGER DEFAULT 0,
    action_object_id INTEGER DEFAULT 0,
    deleted BOOLEAN DEFAULT False,
    type TEXT DEFAULT '',
    blog_media_key TEXT DEFAULT '',
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX notification_user_id_fk ON notification(user_id);

-- +goose Down
DROP INDEX notification_user_id_fk;
DROP TABLE notification;
