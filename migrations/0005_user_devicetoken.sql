
-- +goose Up
-- user_devicetoken table to manage device token for push notifications
CREATE TABLE user_devicetoken(
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE,
    is_active BOOLEAN DEFAULT true,
    token TEXT DEFAULT '',
    type TEXT DEFAULT 'I',
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP

);
CREATE INDEX user_devicetoken_user_id_fk ON user_devicetoken(user_id);

-- +goose Down
DROP INDEX user_devicetoken_user_id_fk;
DROP TABLE user_devicetoken;
