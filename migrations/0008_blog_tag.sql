-- +goose Up
CREATE TABLE blog_tag(
    blog_id INTEGER REFERENCES blog(id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE
);
CREATE INDEX blog_tag_blog_id_fk ON blog_tag(blog_id);
CREATE INDEX blog_tag_user_id_fk ON blog_tag(user_id);
CREATE UNIQUE INDEX blog_tag_user_and_blog_fk ON blog_tag(user_id, blog_id);


-- +goose Down
DROP INDEX blog_tag_user_and_blog_fk;
DROP INDEX blog_tag_blog_id_fk;
DROP INDEX blog_tag_user_id_fk;
DROP TABLE blog_tag;
