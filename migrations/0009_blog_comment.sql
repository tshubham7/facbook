-- +goose Up
CREATE TABLE comment(
    id SERIAL PRIMARY KEY NOT NULL,
    blog_id INTEGER REFERENCES blog(id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE,
    content TEXT DEFAULT '',
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX comment_blog_id_fk ON comment(blog_id);

-- +goose Down

DROP INDEX comment_blog_id_fk;
DROP TABLE comment;
