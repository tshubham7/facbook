
-- +goose Up
-- table to store the complaints from the user 
CREATE TABLE complaint(
    id SERIAL PRIMARY KEY NOT NULL,
    email TEXT DEFAULT '',
    username TEXT DEFAULT '',
    user_id INTEGER REFERENCES facbook_user DEFAULT NULL,
    title TEXT  DEFAULT '',
    checked BOOLEAN  DEFAULT 'false',
    description TEXT  DEFAULT ''
);

CREATE INDEX complaint_email_idx ON complaint(email);
-- +goose Down
DROP INDEX complaint_email_idx;
DROP TABLE complaint;