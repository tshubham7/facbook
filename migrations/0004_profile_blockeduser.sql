-- +goose Up
CREATE TABLE profile_blockeduser(
    owner_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES facbook_user(id) ON DELETE CASCADE
);
CREATE INDEX profile_blockeduser_owner_id_fk ON profile_blockeduser(owner_id);
CREATE INDEX profile_blockeduser_user_id_fk ON profile_blockeduser(user_id);



-- +goose Down
DROP INDEX profile_blockeduser_owner_id_fk;
DROP INDEX profile_blockeduser_user_id_fk;
DROP TABLE profile_blockeduser;


