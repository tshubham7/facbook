-- +goose Up
-- hashtag table to manage blog hashtag that will be used in searching blogs through hashtags
CREATE TABLE hashtag(
    id SERIAL PRIMARY KEY NOT NULL,
    tag TEXT DEFAULT ''
);


-- +goose Down
DROP TABLE hashtag;
