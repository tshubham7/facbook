
-- +goose Up
-- -- this one for encryption
-- CREATE EXTENSION pgcrypto;
-- CREATE EXTENSION postgis;
-- -- Enable Topology
-- CREATE EXTENSION postgis_topology;

CREATE TABLE facbook_user(
    id SERIAL PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    username TEXT NOT NULL UNIQUE,
    email TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL,
    bio TEXT DEFAULT '',
    is_private BOOLEAN DEFAULT false,
    avatar TEXT DEFAULT '',
    last_login timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
);

    

CREATE INDEX facbook_user_username_idx ON facbook_user(username);
CREATE INDEX facbook_user_name_idx ON facbook_user(name);
CREATE INDEX facbook_user_email_idx ON facbook_user(email);


-- +goose Down
DROP INDEX facbook_user_username_idx;
DROP INDEX facbook_user_email_idx;
DROP INDEX facbook_user_name_idx;

DROP TABLE facbook_user;
