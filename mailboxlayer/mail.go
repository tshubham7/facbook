package mailboxlayer

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

var key, q string

func init() {
	key = os.Getenv("FACBOOK_MAILBOXLAYER_KEY")
	q = `http://apilayer.net/api/check?access_key=%s&email=%s&smtp=1&format=1`
}

// CheckEmail function check the validity of email
func CheckEmail(email string) (SMTPCheck bool, FormatValid bool) {
	resp, err := http.Get(fmt.Sprintf(q, key, email))
	if err != nil {
		log.Fatalln("error occured: ", err)
	}
	defer resp.Body.Close()
	var result Response
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		log.Fatalln("error occured: ", err)
	}
	return result.SMTPCheck, result.FormatValid
}
