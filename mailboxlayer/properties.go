package mailboxlayer

// Response struct to hold mailboxlayer api response
type Response struct {
	Email       string  `json:"email"`
	DidYouMean  string  `json:"did_you_mean"`
	User        string  `json:"user"`
	Domain      string  `json:"domain"`
	FormatValid bool    `json:"format_valid"`
	MXFound     bool    `json:"mx_found"`
	SMTPCheck   bool    `json:"smtp_check"`
	CatchAll    bool    `json:"catch_all"`
	Role        bool    `json:"role"`
	Disposable  bool    `json:"disposable"`
	Free        bool    `json:"free"`
	Score       float64 `json:"score"`
}
