to convert main.go file into linux executable
run this command from the root directory of main file
    env GOOS=linux go build -ldflags="-s -w" -o bin/main main.go
    
    a folder will be created named "bin" with the build file

compress the main.go file
run this command
    zip main.zip bin/*

go to the aws console and create a lambda function
upload this compressed file onto it.



references
    https://docs.aws.amazon.com/lambda/latest/dg/lambda-go-how-to-create-deployment-package.html
    https://www.youtube.com/watch?v=2HjcM5KCHD4