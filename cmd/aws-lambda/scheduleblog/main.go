package main

import (
	"log"

	"bitbucket.org/tshubham7/facbook/cmd/facbook-scheduler/scheduler"
	"github.com/aws/aws-lambda-go/lambda"
)

// this will manage the scheduleBlog function
func main() {
	lambda.Start(scheduleBlog)
}

func scheduleBlog() {
	log.Println("Running scheduler for schedule blog")
	scheduler.Tasks().PublishBlog()
}
