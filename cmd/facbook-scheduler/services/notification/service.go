package notification

import (
	"fmt"
	"log"

	"bitbucket.org/tshubham7/facbook/cmd/facbook-scheduler/services"
	"bitbucket.org/tshubham7/facbook/cmd/facbook-scheduler/services/data"
	"github.com/jmoiron/sqlx"
)

// Service ...
type Service interface {
	Save(notificationParams NotificationSave)
}

type notificationStore struct {
	*sqlx.DB
}

// NewService ...
func NewService() Service {
	return &notificationStore{data.DBWrite}
}

// save the notifications this will be used all over the app
func (store *notificationStore) Save(n NotificationSave) {
	keys := services.SQLGenInsertKeys(n)
	values := services.SQLGenInsertValues(n)
	q := fmt.Sprintf(`INSERT INTO notification(%s) VALUES(%s)`, keys, values)
	_, err := store.NamedExec(q, &n)
	if err != nil {
		log.Printf(err.Error())
	}
}
