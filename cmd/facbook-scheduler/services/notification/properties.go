package notification

type NotificationSave struct {
	UserID      string `db:"user_id"`
	Public      bool   `db:"public"`
	Verb        string `db:"verb"`
	ActorObID   string `db:"actor_object_id"`
	ActionObjID string `db:"action_object_id"`
	Description string `db:"description"`
	Type        string `db:"type"`
	Image       string `db:"blog_media_key"`
}
