package bloghelper

import (
	"fmt"
	"strings"

	"bitbucket.org/tshubham7/facbook/cmd/facbook-scheduler/pushnotification"
	"bitbucket.org/tshubham7/facbook/cmd/facbook-scheduler/services/data"
	"github.com/jmoiron/sqlx"
)

// Service ...
type Service interface {
	HandleBlogNotification(userID, blogID string)
}

type blogStore struct {
	*sqlx.DB
}

// New ...
func New() Service {
	return &blogStore{data.DBWrite}
}

// this function will manage all the blog functionalities after publishing the blog
func (store *blogStore) HandleBlogNotification(uid, pid string) {
	username, err := notify().getUsername(uid)
	if err != nil {
		return
	}
	store.NotifiyTaggedUsers(username, uid, pid) // send notification to tagged users
	notify().ToRecipient(uid, pid)               // send notification to the users who have turned on
	store.NotifyMention(username, uid, pid)      // send notification to the mentioned users
}

// send notifications to tagged users
func (store *blogStore) NotifiyTaggedUsers(username, uid, pid string) {
	msg := fmt.Sprintf("%s tagged me in a blog", username)
	q := `SELECT user_id FROM blog_tag WHERE blog_id=$1`
	rows, err := store.Query(q, pid)
	if err != nil {
		return
	}
	var id string
	for rows.Next() {
		rows.Scan(&id)
		if uid != id {
			pushnotification.Push(uid, msg)
		}
	}
}

// extract the mentioned users and send then push notifications
func (store *blogStore) NotifyMention(username, uid, pid string) {
	caption := store.getBlogCaption(pid)
	msg := fmt.Sprintf("%s mentioned you in a blog", username)
	words := strings.SplitAfter(caption, " ")
	usernames := ""
	r := strings.NewReplacer(" ", "")
	for _, u := range words {
		if strings.HasPrefix(u, "@") {
			usernames += "'" + r.Replace(strings.TrimPrefix(u, "@")) + "', "
		}
	}
	if len(usernames) == 0 {
		return
	}
	usernames = usernames[0 : len(usernames)-2]
	q := fmt.Sprintf("select id from facbook_user where username in (%s)", usernames)
	rows, _ := store.Query(q)
	var id string
	for rows.Next() {
		rows.Scan(&id)
		if uid != id {
			pushnotification.Push(id, msg)
		}
	}
}

// return blog caption
func (store *blogStore) getBlogCaption(pid string) string {
	var caption string
	q := `SELECT caption FROM blog WHERE id=$1`
	store.QueryRow(q, pid).Scan(&caption)
	return caption
}
