package bloghelper

import (
	"bitbucket.org/tshubham7/facbook/cmd/facbook-scheduler/services/data"
	"github.com/jmoiron/sqlx"
)

// Notify ...
type Notify interface {
	getUsername(blogID string) (string, error)
}

type notifyStore struct {
	*sqlx.DB
}

func notify() Notify {
	return &notifyStore{data.DBWrite}
}

// get the username of the user who blogged the blog
func (store *notifyStore) getUsername(uid string) (string, error) {
	var username string
	q := `SELECT username FROM facbook_user WHERE id=$1`
	err := store.QueryRow(q, uid).Scan(&username)
	return username, err
}
