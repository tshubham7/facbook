package scheduler

import (
	"fmt"
	"log"

	"bitbucket.org/tshubham7/facbook/cmd/facbook-scheduler/pushnotification"
	helper "bitbucket.org/tshubham7/facbook/cmd/facbook-scheduler/services/bloghelper"
	"bitbucket.org/tshubham7/facbook/cmd/facbook-scheduler/services/data"
	"github.com/jmoiron/sqlx"
)

type Task interface {
	Test()
	PublishBlog()
}

type store struct {
	*sqlx.DB
}

// Tasks ...
func Tasks() Task {
	return &store{data.DBWrite}
}

func (st *store) Test() {
	log.Println("Testing scheduler...")
}

// this task will publish scheduled blogs
func (st *store) PublishBlog() {
	q := `UPDATE blog SET is_active=true, created_at=CURRENT_TIMESTAMP 
		WHERE is_active=false AND (scheduled_time < CURRENT_TIMESTAMP OR scheduled_time IS NULL)
		RETURNING id, user_id
	`
	rows, _ := st.Query(q)
	msg := "my scheduled blog is activated"
	var id, uid string
	for rows.Next() {
		rows.Scan(&id, &uid)
		fmt.Println(id, uid)
		pushnotification.Push(uid, msg)
		helper.New().HandleBlogNotification(uid, id)
	}
}
