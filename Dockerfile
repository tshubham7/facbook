FROM golang:1.13

WORKDIR /src
COPY . /src

RUN go build -o facbook

EXPOSE 8080
CMD [ "./facbook" ]
