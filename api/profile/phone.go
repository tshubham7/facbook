package profile

import (
	"log"
	"net/http"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"
	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/profile"
	"github.com/go-chi/render"
)

// update user phone number
func (rt *router) UpdatePhone(w http.ResponseWriter, r *http.Request) {
	var params profile.Params
	if err := render.Decode(r, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	if ok := ValidatePhone(&params.Mobile); !ok {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid phone number", "error": "invalid number"})
		return
	}
	cuid := mid.UserID(r)
	if err := rt.service.UpdatePhone(cuid, params.Mobile); err == nil {
		render.JSON(w, r, services.M{"message": "otp sent to the number"})
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": ParseProfileUpdateError(err), "error": err.Error()})
	}
}

// verify user phone number
func (rt *router) VerifyPhone(w http.ResponseWriter, r *http.Request) {
	var params struct {
		Otp    string `json:"otp"`
		Mobile string `json:"mobile"`
	}
	if err := render.Decode(r, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	if ok := ValidatePhone(&params.Mobile); !ok {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid phone number", "error": "invalid number"})
		return
	}

	uid := mid.UserID(r)
	if err := rt.service.VerifyPhone(uid, params.Mobile, params.Otp); err == nil {
		render.JSON(w, r, services.M{"message": "successfully verified"})
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": ParseProfileUpdateError(err), "error": err.Error()})
	}
}
