package profile

import (
	"log"
	"net/http"
	"strings"

	"github.com/go-chi/render"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"
	"bitbucket.org/tshubham7/facbook/properties"
	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/auth"
	"bitbucket.org/tshubham7/facbook/services/profile"
	"bitbucket.org/tshubham7/facbook/services/user"
	"github.com/go-chi/chi"
)

// Router interface
type Router interface {
	Mount(*chi.Mux)
	GetByID(http.ResponseWriter, *http.Request)
	GetByUsername(http.ResponseWriter, *http.Request)
	Update(http.ResponseWriter, *http.Request)
	UpdateAvatar(http.ResponseWriter, *http.Request)
	CopyLink(http.ResponseWriter, *http.Request)
	UpdatePhone(http.ResponseWriter, *http.Request)
	VerifyPhone(http.ResponseWriter, *http.Request)
	UpdateEmail(http.ResponseWriter, *http.Request)
	VerifyEmail(http.ResponseWriter, *http.Request)
}
type router struct {
	service profile.Service
}

// New function to route these services
func New() Router {
	return &router{profile.NewService()}
}

func (rt *router) routes() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/", rt.GetByID)
	r.Get("/by-username", rt.GetByUsername)
	r.Patch("/", rt.Update)
	r.Patch("/avatar", rt.UpdateAvatar)
	r.Post("/turn-on", rt.TurnOnNotification)
	r.Post("/turn-off", rt.TurnOffNotification)

	r.Patch("/phone/otp", rt.UpdatePhone)
	r.Patch("/phone/verify", rt.VerifyPhone)

	r.Patch("/email/otp", rt.UpdateEmail)
	r.Patch("/email/verify", rt.VerifyEmail)

	r.Post("/", rt.Delete)
	return r
}

func (rt *router) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Use(mid.Authenticator())
		ro.Use(render.SetContentType(render.ContentTypeJSON))
		ro.Mount("/api/user/{id}/profile", rt.routes())
	})
}

// get user profile by the user id
func (rt *router) GetByID(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	cuid := mid.UserID(r)
	if p, err := rt.service.GetByID(cuid, id); err == nil {
		render.JSON(w, r, p)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": ParseProfileGetError(err), "error": err.Error()})
	}
}

// get user profile by the username
func (rt *router) GetByUsername(w http.ResponseWriter, r *http.Request) {
	username := chi.URLParam(r, "id") // username will be passed
	cuid := mid.UserID(r)
	if p, err := rt.service.GetByUsername(cuid, username); err == nil {
		render.JSON(w, r, p)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": ParseProfileGetError(err), "error": err.Error()})
	}
}

// update user profile by user id
func (rt *router) Update(w http.ResponseWriter, r *http.Request) {
	uid := chi.URLParam(r, "id")
	cuid := mid.UserID(r) // retrieve from header auth token
	if uid != cuid {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "access denied",
			"error": "can not update other user profile"})
		return
	}
	var params profile.Update
	if err := render.Decode(r, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	if p, err := rt.service.Update(cuid, params); err == nil {
		render.JSON(w, r, p)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": ParseProfileUpdateError(err), "error": err.Error()})
	}
}

// udpate user profile picture
func (rt *router) UpdateAvatar(w http.ResponseWriter, r *http.Request) {
	var params profile.Params
	if err := render.Decode(r, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	if params.Avatar == "" {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid input for key: avatar",
			"error": "blank string found in key: avatar"})
		return
	}
	cuid := mid.UserID(r)
	if k, err := rt.service.UpdateAvatar(cuid, params.Avatar); err == nil {
		render.JSON(w, r, services.M{"message": "successfully updated", "image": k})
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// copy user profile link
func (rt *router) CopyLink(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": "invalid user id"})
		return
	}
	link := properties.DeepLinkUrl + "/user/" + id + "profile"
	render.JSON(w, r, services.M{"link": link})
}

// list of user all contacts (followings and followers)
func (rt *router) AllContact(w http.ResponseWriter, r *http.Request) {
	uid := chi.URLParam(r, "id")
	if c, err := rt.service.AllContact(uid); err == nil {
		render.JSON(w, r, c)
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// turn on profile notification
func (rt *router) TurnOnNotification(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r)
	uid := chi.URLParam(r, "id")
	if err := rt.service.TurnOnNotification(cuid, uid); err == nil {
		render.JSON(w, r, services.M{"message": "notification turned on"})
	} else {
		sts := 500
		msg := "something went wrong"
		if strings.Contains(err.Error(), "access denied") {
			msg = "you can not perform this action on private profile"
			sts = 400
		}
		w.WriteHeader(sts)
		render.JSON(w, r, services.M{"message": msg, "error": err.Error()})
	}
}

// turn off profile notification
func (rt *router) TurnOffNotification(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r)
	uid := chi.URLParam(r, "id")
	if err := rt.service.TurnOffNotification(cuid, uid); err == nil {
		render.JSON(w, r, services.M{"message": "notification turned off"})
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// Delete handler function for delete user api
func (rt *router) Delete(w http.ResponseWriter, r *http.Request) {
	var params struct {
		Password string `json:"password"`
	}
	cuid := mid.UserID(r) // should be the current logged in user
	// cuid := "1382"
	uid := chi.URLParam(r, "id")

	if uid != cuid {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "action not allowed",
			"error": "action not allowed, can't delete other user"})
		return
	}
	if err := render.Decode(r, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r,
			services.M{"message": "missing or invalid param: password",
				"error": err.Error()})
		return
	}
	var p string
	var err error
	if p, err = user.NewService().GetPasswordByID(cuid); err != nil {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
		return
	}

	if ok := auth.New().IsPasswordValid(p, params.Password); !ok {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid password", "error": "wrong password"})
		return
	}

	if err = user.NewService().Delete(cuid); err != nil {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
		return
	}
	render.JSON(w, r, services.M{"message": "successfully deleted"})
	// expire the auth token
}
