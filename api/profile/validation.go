package profile

import (
	"strings"

	"github.com/ttacon/libphonenumber"
)

// ParseProfileGetError parsing error of profile get api service
func ParseProfileGetError(err error) string {

	if strings.Contains(err.Error(), "no rows in result set") {
		return "invalid id/username or record does not exists"
	}
	return "something went wrong"
}

// ParseProfileUpdateError parsing error of profile update api service
func ParseProfileUpdateError(err error) string {
	if strings.Contains(err.Error(), "facbook_user_email_key") {
		return "email already exists, try another one?"
	}
	if strings.Contains(err.Error(), "facbook_user_username_key") {
		return "username already exists, try another one?"
	}
	if strings.Contains(err.Error(), "Number_Unvailable") {
		return "this number is not available/already taken, try another one?"
	}
	return "something went wrong"
}

// ValidatePhone validate phone number
// gives back the formatted the number
func ValidatePhone(s *string) bool {
	if s == nil {
		return true
	}
	num, err := libphonenumber.Parse(*s, "US")
	if err != nil {
		return false
	}
	ok := libphonenumber.IsValidNumber(num) // check if number is valid
	if ok {
		*s = libphonenumber.Format(num, libphonenumber.INTERNATIONAL)
	}
	return ok
}
