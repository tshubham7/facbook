package blog

import (
	"log"
	"net/http"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"

	"bitbucket.org/tshubham7/facbook/properties"
	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/blog"
)

// Router interface...
type Router interface {
	Create(http.ResponseWriter, *http.Request)
	GetByID(http.ResponseWriter, *http.Request)
	TaggedUsers(http.ResponseWriter, *http.Request)
	Delete(http.ResponseWriter, *http.Request)
	Mount(*chi.Mux)
}

type router struct {
	service blog.Service
}

// New router function...
func New() Router {
	return &router{blog.NewService()}
}

func (route *router) routes() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/{id}", route.GetByID)
	r.Delete("/{id}", route.Delete)
	r.Patch("/{id}", route.Update)
	r.Post("/", route.Create)
	r.Get("/{id}/link", route.CopyLink)
	r.Get("/{id}/tag", route.TaggedUsers)
	return r
}

func (route *router) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Use(mid.Authenticator())
		ro.Use(render.SetContentType(render.ContentTypeJSON))
		ro.Mount("/api/blog", route.routes())
	})
}

// create a blog
func (route *router) Create(w http.ResponseWriter, r *http.Request) {
	var params blog.BlogParam
	if err := render.Decode(r, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	params.UserID = mid.UserID(r)
	if id, err := route.service.Create(params); err == nil {
		render.JSON(w, r, services.M{"message": "successfully created", "id": id})
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// update a blog
func (route *router) Update(w http.ResponseWriter, r *http.Request) {
	var params blog.BlogParam
	if err := render.Decode(r, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	id := chi.URLParam(r, "id")
	params.UserID = mid.UserID(r)
	if p, err := route.service.Update(id, params); err == nil {
		render.JSON(w, r, p)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// copy blog link
func (route *router) CopyLink(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": "invalid blog id"})
		return
	}
	link := properties.DeepLinkUrl + "/blog/" + id
	render.JSON(w, r, services.M{"link": link})
}

// delete a blog
func (route *router) Delete(w http.ResponseWriter, r *http.Request) {
	blogID := chi.URLParam(r, "id")
	if err := route.service.Delete(blogID); err == nil {
		render.JSON(w, r, services.M{"message": "successfully deleted"})
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// get the complete detail of blog
func (route *router) GetByID(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	cuid := mid.UserID(r)
	if p, err := route.service.ByID(cuid, id); err == nil {
		render.JSON(w, r, p)
	} else {
		log.Println(err)
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": ParseGetError(err), "error": err.Error()})
	}
}

// list blog for home screen
func (route *router) TaggedUsers(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r)
	blogID := chi.URLParam(r, "id")
	if list, err := route.service.TaggedUsers(cuid, blogID); err == nil {
		render.JSON(w, r, list)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}
