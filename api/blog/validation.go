package blog

import (
	"database/sql"
)

// ParseGetError ...
func ParseGetError(err error) string {
	if err == sql.ErrNoRows {
		return "invalid blog id"
	}
	return "something went wrong"
}
