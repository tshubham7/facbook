package feed

import (
	"log"
	"net/http"
	"strconv"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"
	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/feed"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

// Router interface for routes of feed app
type Router interface {
	Feed(http.ResponseWriter, *http.Request)
	ByType(http.ResponseWriter, *http.Request)
	Mount(*chi.Mux)
}

type router struct {
	service feed.Service
}

// New function for handler functions of feed app
func New() Router {
	return &router{feed.NewService()}
}

func (route *router) routes() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/", route.Feed)
	r.Get("/type", route.ByType)
	r.Get("/near-me", route.NearMe)
	return r
}

func (route *router) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Use(mid.Authenticator())
		ro.Use(render.SetContentType(render.ContentTypeJSON))
		ro.Mount("/api/feed", route.routes())
	})
}

// list post for home screen
func (route *router) Feed(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r)
	p, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}

	var resp Response
	if resp.Data, err = route.service.Feed(cuid, p); err == nil {
		render.JSON(w, r, resp)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}
