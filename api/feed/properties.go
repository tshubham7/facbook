package feed

// Response struct for list response
type Response struct {
	Data interface{} `json:"data"`
}
