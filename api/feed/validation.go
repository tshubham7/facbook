package feed

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/Self-in/api-core/services"
	"github.com/go-chi/render"
)

// check the parameters if it's wrong or not
func (rt *router) fetchParams(w http.ResponseWriter, r *http.Request) (float64, float64, float64, error) {
	var radius, longitude, latitude float64
	var err error
	if radius, err = strconv.ParseFloat(
		strings.TrimSpace(r.URL.Query().Get("radius")), 64); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "missing or invalid radius param", "error": err.Error()})
		return radius, longitude, latitude, err
	}
	if longitude, err = strconv.ParseFloat(
		strings.TrimSpace(r.URL.Query().Get("longitude")), 64); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "missing or invalid longitude param", "error": err.Error()})
		return radius, longitude, latitude, err
	}

	if latitude, err = strconv.ParseFloat(
		strings.TrimSpace(r.URL.Query().Get("latitude")), 64); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "missing or invalid latitude param", "error": err.Error()})
		return radius, longitude, latitude, err
	}
	return radius, longitude, latitude, nil
}
