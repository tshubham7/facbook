package complaint

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"

	"github.com/go-chi/render"

	"github.com/Self-in/api-core/services"
	"github.com/Self-in/api-core/services/complaint"
)

type Router interface {
	Create(http.ResponseWriter, *http.Request)
	Mount(*chi.Mux)
}

type router struct {
	service complaint.Service
}

func New() Router {
	return &router{complaint.NewService()}
}

func (rt *router) routes() *chi.Mux {
	r := chi.NewRouter()
	r.Post("/", rt.Create)
	r.Get("/", rt.Complaints)
	r.Patch("/{id}/check", rt.Check)
	return r
}

func (rt *router) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Mount("/api/complaint", rt.routes())
	})
}

// create a complaint
func (rt *router) Create(w http.ResponseWriter, r *http.Request) {
	var params complaint.Create
	if err := render.Decode(r, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	if err := rt.service.Create(params); err != nil {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	} else {
		render.JSON(w, r, services.M{"message": "successfully created"})
	}
}

// list complaints
func (rt *router) Complaints(w http.ResponseWriter, r *http.Request) {
	if c, err := rt.service.Complaints(); err != nil {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	} else {
		render.JSON(w, r, c)
	}
}

// mark check complaint
func (rt *router) Check(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if c, err := rt.service.Check(id); err != nil {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	} else {
		render.JSON(w, r, c)
	}
}
