package notification

import (
	"log"
	"net/http"
	"strconv"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/notification"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

type Router interface {
	SelfList(http.ResponseWriter, *http.Request)
	FollowRequests(http.ResponseWriter, *http.Request)
	Mount(*chi.Mux)
}

type router struct {
	service notification.Service
}

func New() Router {
	return &router{notification.NewService()}
}

func (rt *router) routes() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/", rt.List)
	r.Get("/follow-request", rt.FollowRequests)
	return r
}

func (rt *router) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Use(mid.Authenticator())
		ro.Use(render.SetContentType(render.ContentTypeJSON))
		ro.Mount("/api/notification", rt.routes())
	})
}

func (rt *router) List(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r)
	p, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}

	if list, err := rt.service.List(cuid, p); err == nil {
		render.JSON(w, r, list)
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// list of the following requests
func (rt *router) FollowRequests(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r)
	if list, err := rt.service.FollowRequests(cuid); err == nil {
		render.JSON(w, r, list)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}
