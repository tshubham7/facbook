package userblog

import (
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/tshubham7/facbook/services/userblog"
	"github.com/go-chi/render"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"
	"bitbucket.org/tshubham7/facbook/services"
	"github.com/go-chi/chi"
)

// Router ...
type Router interface {
	Mount(*chi.Mux)
	Blogs(http.ResponseWriter, *http.Request)
}

type router struct {
	service userblog.Service
}

func (rt *router) routes() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/blog", rt.Blogs)
	return r
}

// New ...
func New() Router {
	return &router{userblog.NewService()}
}

func (rt *router) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Use(mid.Authenticator())
		ro.Use(render.SetContentType(render.ContentTypeJSON))
		ro.Mount("/api/user/{id}/blog", rt.routes())
	})
}

// Blogs handler functions for user blog list api
func (rt *router) Blogs(w http.ResponseWriter, r *http.Request) {
	p, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	uid := chi.URLParam(r, "id")
	if list, err := rt.service.Blogs(uid, p); err == nil {
		render.JSON(w, r, list)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}
