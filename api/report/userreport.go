package report

import (
	"net/http"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/report"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

// UserRouter ...
type UserRouter interface {
	Mount(*chi.Mux)
	Create(http.ResponseWriter, *http.Request)
}

type userRouter struct {
	service report.UserService
}

// NewUserRouter ...
func NewUserRouter() UserRouter {
	return &userRouter{report.NewUserService()}
}

func (rt *userRouter) routes() *chi.Mux {
	r := chi.NewRouter()
	r.Post("/", rt.Create)
	return r
}

func (rt *userRouter) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Use(mid.Authenticator())
		ro.Use(render.SetContentType(render.ContentTypeJSON))
		ro.Mount("/api/user/{id}/report", rt.routes())
	})
}

// create report for user
func (rt *userRouter) Create(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r)
	// cuid := "1133"
	var params report.UserReport
	if err := render.Decode(r, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
		return
	}
	params.UserID = chi.URLParam(r, "id")
	params.ReporterID = cuid
	if err := report.NewUserService().Create(params); err == nil {
		msg := `Thank you for helping us improve facbook.
		We will investigate this user and take appropriate action with in 24 hours.`
		render.JSON(w, r, services.M{"message": msg})
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": ParseError(err), "error": err.Error()})
	}
}
