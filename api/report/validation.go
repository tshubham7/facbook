package report

import "strings"

// ParseError parse the api service error
func ParseError(err error) string {

	if strings.Contains(err.Error(), "user_report_user_id_fkey") || strings.Contains(err.Error(), "blog_report_blog_id_fkey") {
		return "invalid id or record does not exits"
	}
	return "something went wrong"
}
