package report

import (
	"log"
	"net/http"

	"github.com/go-chi/render"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"
	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/report"
	"github.com/go-chi/chi"
)

// BlogRouter ...
type BlogRouter interface {
	Mount(*chi.Mux)
	Create(http.ResponseWriter, *http.Request)
}

type blogRouter struct {
	service report.BlogService
}

// NewBlogRouter ...
func NewBlogRouter() BlogRouter {
	return &blogRouter{report.NewBlogService()}
}

func (rt *blogRouter) routes() *chi.Mux {
	r := chi.NewRouter()
	r.Post("/", rt.Create)
	return r
}

func (rt *blogRouter) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Use(mid.Authenticator())
		ro.Use(render.SetContentType(render.ContentTypeJSON))
		ro.Mount("/api/blog/{id}/report", rt.routes())
	})
}

// create report for blog
func (rt *blogRouter) Create(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r)
	var params report.BlogReport

	if err := render.Decode(r, &params); err != nil {
		w.WriteHeader(400)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "error encountered", "error": err.Error()})
		return
	}
	params.BlogID = chi.URLParam(r, "id")
	params.ReporterID = cuid
	if err := rt.service.Create(params); err == nil {
		msg := `Thank you for helping us improve facbook.
		We will investigate this blog and take appropriate action with in 24 hours.`
		render.JSON(w, r, services.M{"message": msg})
	} else {
		log.Println(err)
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": ParseError(err), "error": err.Error()})
	}
}
