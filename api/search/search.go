package search

import (
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/render"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"
	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/search"
	"github.com/go-chi/chi"
)

// Router ...
type Router interface {
	User(http.ResponseWriter, *http.Request)
	Hashtag(http.ResponseWriter, *http.Request)
	Location(http.ResponseWriter, *http.Request)
	Mount(*chi.Mux)
}

type router struct {
	service search.Service
}

func (rt *router) routes() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/user", rt.User)
	r.Get("/hashtag", rt.Hashtag)
	r.Get("/location", rt.Location)
	return r
}

// New ...
func New() Router {
	return &router{search.NewService()}
}

func (rt *router) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Use(mid.Authenticator())
		ro.Use(render.SetContentType(render.ContentTypeJSON))
		ro.Mount("/api/search", rt.routes())
	})
}

// search user list
func (rt *router) User(w http.ResponseWriter, r *http.Request) {
	qp := r.URL.Query().Get("query")
	pg, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	cuid := mid.UserID(r)
	// cuid := "1133"
	if p, err := rt.service.User(cuid, qp, pg); err == nil {
		render.JSON(w, r, p)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// search location
func (rt *router) Location(w http.ResponseWriter, r *http.Request) {
	qp := r.URL.Query().Get("query")
	pg, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	if locs, err := rt.service.Location(qp, pg); err == nil {
		render.JSON(w, r, locs)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// search hashtags
func (rt *router) Hashtag(w http.ResponseWriter, r *http.Request) {
	pg, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	qp := r.URL.Query().Get("query")
	if tags, err := rt.service.Hashtag(qp, pg); err == nil {
		render.JSON(w, r, tags)
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}

}
