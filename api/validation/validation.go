package validation

import "strings"

// ParseAPIError parse the validation api error messages
func ParseAPIError(err error, s string) (string, int) {
	if strings.Contains(err.Error(), "already exists in the db") {
		return s + " already exists", 400
	}
	return "something went wrong", 500
}
