package validation

import (
	"fmt"
	"net/http"

	"bitbucket.org/tshubham7/facbook/api/utility"
	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/validation"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

// Router interface implements the handler functions for validation app
type Router interface {
	Username(http.ResponseWriter, *http.Request)
	Email(http.ResponseWriter, *http.Request)
	Phone(http.ResponseWriter, *http.Request)
	Mount(*chi.Mux)
}

type router struct {
	service validation.Service
}

// New function manages the handler functions for validation app
func New() Router {
	return &router{validation.New()}
}

func (route *router) routes() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/username", route.Username)
	r.Get("/email", route.Email)
	r.Get("/phone", route.Phone)
	return r
}

func (route *router) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Mount("/api/validation", route.routes())
	})
}

// check the existance of username in the database
func (route *router) Username(w http.ResponseWriter, r *http.Request) {
	u := r.URL.Query().Get("query")
	if u == "" {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "mising or invalid params", "error": "blank username"})
		return
	}
	cuid := r.URL.Query().Get("userID")
	if err := route.service.Username(cuid, u); err != nil {
		m, s := ParseAPIError(err, "username")
		w.WriteHeader(s)
		render.JSON(w, r, services.M{"message": m,
			"error": err.Error()})
	} else {
		render.JSON(w, r, services.M{"message": "username is valid"})
	}
}

// check the existance of email in the database
func (route *router) Email(w http.ResponseWriter, r *http.Request) {
	u := r.URL.Query().Get("query")
	if u == "" {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "mising or invalid params", "error": "blank username"})
		return
	}
	cuid := r.URL.Query().Get("userID")
	if err := route.service.Email(cuid, u); err != nil {
		m, s := ParseAPIError(err, "email")
		w.WriteHeader(s)
		render.JSON(w, r, services.M{"message": m,
			"error": err.Error()})
	} else {
		render.JSON(w, r, services.M{"message": "email is valid"})
	}
}

// check the existance of phone number in the database
func (route *router) Phone(w http.ResponseWriter, r *http.Request) {
	m := r.URL.Query().Get("query")
	m = "+" + m
	fmt.Println(m)
	if ok := utility.ValidatePhone(&m); m == "" || !ok {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid phone number", "error": "invalid number"})
		return
	}
	cuid := r.URL.Query().Get("userID")
	if err := route.service.PhoneNumber(cuid, m); err != nil {
		m, s := ParseAPIError(err, "phone number")
		w.WriteHeader(s)
		render.JSON(w, r, services.M{"message": m,
			"error": err.Error()})
	} else {
		render.JSON(w, r, services.M{"message": "phone number is valid"})
	}
}
