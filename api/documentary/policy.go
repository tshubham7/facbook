package documentary

import (
	"bytes"
	"html/template"
	"net/http"

	"github.com/go-chi/render"

	"github.com/go-chi/chi"
)

type Router struct {
}

func New() *Router {
	return &Router{}
}

func (rt *Router) routes() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/terms-of-use", rt.TermsOfUse)
	r.Get("/contact-us", rt.ContactUs)
	r.Get("/FAQs", rt.FAQs)
	r.Get("/terms-and-policy", rt.TermsAndPolicyCombine)
	r.Get("/privacy-policy", rt.PrivacyPolicy)
	// r.Post("/", rt.Terms)
	return r
}

func (rt *Router) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Mount("/api/facbook", rt.routes())
	})
}

func renderToString(file string) string {
	t, _ := template.ParseFiles(file)
	var tmpl bytes.Buffer
	t.Execute(&tmpl, nil)
	return tmpl.String()
}

func (c *Router) TermsOfUse(w http.ResponseWriter, r *http.Request) {
	t := renderToString("templates/static_templates/terms_of_use.html")
	render.HTML(w, r, t)
}

func (c *Router) ContactUs(w http.ResponseWriter, r *http.Request) {
	t := renderToString("templates/static_templates/contact_us.html")
	render.HTML(w, r, t)
}

func (c *Router) FAQs(w http.ResponseWriter, r *http.Request) {
	t := renderToString("templates/static_templates/FAQs.html")
	render.HTML(w, r, t)
}

func (c *Router) TermsAndPolicyCombine(w http.ResponseWriter, r *http.Request) {
	t := renderToString("templates/static_templates/terms_and_policies_combine.html")
	render.HTML(w, r, t)
}

func (c *Router) PrivacyPolicy(w http.ResponseWriter, r *http.Request) {
	t := renderToString("templates/static_templates/privacy_policy.html")
	render.HTML(w, r, t)
}
