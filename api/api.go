package api

import (
	"io/ioutil"
	"log"
	"net/http"

	"bitbucket.org/tshubham7/facbook/api/auth"
	"bitbucket.org/tshubham7/facbook/api/blog"
	"bitbucket.org/tshubham7/facbook/api/blogreaction"
	"bitbucket.org/tshubham7/facbook/api/complaint"
	"bitbucket.org/tshubham7/facbook/api/feed"
	"bitbucket.org/tshubham7/facbook/api/follow"
	mid "bitbucket.org/tshubham7/facbook/api/middleware"
	"bitbucket.org/tshubham7/facbook/api/notification"
	"bitbucket.org/tshubham7/facbook/api/profile"
	"bitbucket.org/tshubham7/facbook/api/report"
	"bitbucket.org/tshubham7/facbook/api/scheduledblog"
	"bitbucket.org/tshubham7/facbook/api/search"
	"bitbucket.org/tshubham7/facbook/api/validation"
	"bitbucket.org/tshubham7/facbook/services"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"
	"golang.org/x/text/search"
)

var router *chi.Mux

func init() {
	router = chi.NewRouter()
}

func addMiddleware() {
	router.Use(
		mid.Verifier(),
		render.SetContentType(render.ContentTypeJSON),
		middleware.Logger,
		middleware.DefaultCompress,
		middleware.RedirectSlashes,
		middleware.Recoverer,
	)
}

func mountRoutes() {

	addMiddleware()

	feed.New().Mount(router)
	auth.New().Mount(router)
	complaint.New().Mount(router)
	profile.New().Mount(router)
	search.New().Mount(router)
	validation.New().Mount(router)
	blog.New().Mount(router)
	scheduledblog.New().Mount(router)
	blogreaction.NewLikeRouter().Mount(router)
	blogreaction.NewCommentRouter().Mount(router)
	follow.New().Mount(router)
	notification.New().Mount(router)
	report.NewBlogRouter().Mount(router)
	report.NewUserRouter().Mount(router)
	userblog.New().Mount(router)
	router.Get("/", index)
	router.Get("/apple-app-site-association", appleAssociatedSites)
}

func index(w http.ResponseWriter, r *http.Request) {
	render.JSON(w, r, services.M{"message": "Welcome to Facbook"})
}

func appleAssociatedSites(w http.ResponseWriter, r *http.Request) {

	requestJSON, err := ioutil.ReadFile("./apple-app-site-association")
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("unable to response with data"))
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Write(requestJSON)
}

//Start function will start the services
// and listen on 8080 port
func Start() {
	cors := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
	})
	router.Use(cors.Handler)
	mountRoutes()
	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		log.Printf("%s %s\n", method, route)
		return nil
	}

	if err := chi.Walk(router, walkFunc); err != nil {
		log.Panicf("loggin error: %s\n", err.Error())
	}
	log.Println("exposing on port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
