package scheduledblog

import (
	"log"
	"net/http"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/scheduledblog"
)

// Router ...
type Router interface {
	List(http.ResponseWriter, *http.Request)
	Mount(*chi.Mux)
}

type router struct {
	service scheduledblog.Service
}

// New ...
func New() Router {
	return &router{scheduledblog.NewService()}
}

func (route *router) routes() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/", route.List)
	r.Patch("/{id}", route.Update)
	r.Patch("/{id}/share-now", route.ShareNow)
	return r
}

func (route *router) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Use(mid.Authenticator())
		ro.Use(render.SetContentType(render.ContentTypeJSON))
		ro.Mount("/api/scheduled-blog", route.routes())
	})
}

// list of scheduled blog for this user
func (route *router) List(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r)
	if p, err := route.service.List(cuid); err == nil {
		render.JSON(w, r, p)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// list of scheduled blog for this user
func (route *router) ShareNow(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r)
	blogID := chi.URLParam(r, "id")
	if err := route.service.ShareNow(cuid, blogID); err == nil {
		render.JSON(w, r, services.M{"message": "blog shared"})
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// update the scheduled blog
func (route *router) Update(w http.ResponseWriter, r *http.Request) {
	blogID := chi.URLParam(r, "id")
	cuid := mid.UserID(r)
	var params scheduledblog.UpdateBlogParam
	if err := render.Decode(r, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	if _, err := route.service.Update(cuid, blogID, params); err == nil {
		render.JSON(w, r, services.M{"message": "successfully updated"}) // don't need the response now
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}
