package utility

import "github.com/ttacon/libphonenumber"

// ValidatePhone validate phone number
// gives back the formatted the number
func ValidatePhone(s *string) bool {
	if s == nil {
		return true
	}
	num, err := libphonenumber.Parse(*s, "US")
	if err != nil {
		return false
	}
	ok := libphonenumber.IsValidNumber(num) // check if number is valid
	if ok {
		*s = libphonenumber.Format(num, libphonenumber.INTERNATIONAL)
	}
	return ok
}
