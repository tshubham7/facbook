package auth

import (
	"strings"

	"github.com/ttacon/libphonenumber"
)

// ValidatePhone function validate phone number
// gives back the formatted the number
func ValidatePhone(s string) (string, bool) {
	if s == "" {
		return s, true
	}
	num, err := libphonenumber.Parse(s, "US")
	if err != nil {
		return s, false
	}
	ok := libphonenumber.IsValidNumber(num) // check if number is valid
	if ok {
		return libphonenumber.Format(num, libphonenumber.INTERNATIONAL), true
	}
	return s, ok
}

// parse error
func parseSignupError(err error) string {
	if strings.Contains(err.Error(), "cre_user_email_key") {
		return "email already exists"
	}
	if strings.Contains(err.Error(), "cre_user_phone_key") {
		return "phone number already registered"
	}
	return "something went wrong"

}

// ParseForgotPasswordAPIError function parse the forgot pass api error
func ParseForgotPasswordAPIError(err error) (httpCode int, errorMessage string) {
	if strings.Contains(err.Error(), "notExist") {
		return 400, "email does not exist."
	}
	return 500, "something went wrong"
}
