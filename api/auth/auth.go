package auth

import (
	"fmt"
	"log"
	"net/http"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"

	"bitbucket.org/tshubham7/facbook/services"

	"bitbucket.org/tshubham7/facbook/services/auth"
	"bitbucket.org/tshubham7/facbook/services/devicetoken"
	"bitbucket.org/tshubham7/facbook/services/user"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

var authService = auth.New()
var userService = user.New()

//Routes return a router when all the auth related endpoints are registered
func routes() *chi.Mux {
	router := chi.NewRouter()
	router.Post("/register", register)
	router.Post("/login", login)
	router.Post("/logout", logout)

	router.Post("/reset-password/email", forgotPassword)
	router.Post("/change-password", changePassword)
	return router
}

//Mount the auth routes to the passed `r`
func Mount(r *chi.Mux) {
	r.Mount("/api/auth", routes())
}

//Register route creates and authenticates an user,
// it sends back the newly created user and an auth token
func register(w http.ResponseWriter, r *http.Request) {
	var params user.CreateParam
	var resp struct {
		Token  auth.Authtoken `json:"token"`
		UserID string         `json:"userId"`
	}

	if err := render.DecodeJSON(r.Body, &params); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Printf(err.Error())
		render.JSON(w, r, services.M{"message": "invalid or missing params",
			"error": err.Error()})
		return
	}
	var ok bool
	if params.Phone, ok = ValidatePhone(params.Phone); !ok {
		w.WriteHeader(400)
		render.JSON(w, r,
			services.M{
				"message": "invalid phone number",
				"error":   fmt.Sprintf("invalid phone number: %s", params.Phone)})
		return
	}

	id, err := userService.Register(params)
	if err != nil {
		w.WriteHeader(500)
		msg := parseSignupError(err)
		log.Println(msg, err.Error())
		render.JSON(w, r, services.M{"message": msg, "error": err.Error()})
		return
	}
	resp.Token = authService.Encode(id, params.Email)
	resp.UserID = id
	render.JSON(w, r, resp)
}

// Login route handles user login endpoint
func login(w http.ResponseWriter, r *http.Request) {
	var params user.LoginParam
	var resp struct {
		Token  auth.Authtoken `json:"token"`
		UserID string         `json:"userId"`
	}
	if err := render.DecodeJSON(r.Body, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "missing or invalid params", "error": err.Error()})
		return
	}
	u, err := userService.Login(params.Email)
	if err != nil {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
		return
	}
	if ok := authService.IsPasswordValid(u.Password, params.Password); !ok {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid credentials", "error": "invalid password"})
		return
	}
	resp.Token = authService.Encode(u.ID, u.Email)
	resp.UserID = u.ID
	if params.DeviceToken != "" {
		go devicetoken.New().CreateOrUpdate(u.ID, params.DeviceToken) // updating device token
	}
	render.JSON(w, r, resp)
}

// Logout route handles user login endpoint
func logout(w http.ResponseWriter, r *http.Request) {
	var params user.LoginParam
	var err error
	if err = render.DecodeJSON(r.Body, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "missing or invalid params", "error": err.Error()})
		return
	}
	var uid string

	if uid, err = mid.UserIDWithError(r); err != nil {
		w.WriteHeader(401)
		render.JSON(
			w, r,
			services.M{"message": "invalid auth token", "error": "invalid token"})
		return
	}
	if err := userService.Logout(uid, params.DeviceToken); err != nil {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
		return
	}
	render.JSON(w, r, services.M{"message": "successfully logged out"})
}

// forgot pass
func forgotPassword(w http.ResponseWriter, r *http.Request) {
	var params user.LoginParam
	if err := render.DecodeJSON(r.Body, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "missing or invalid params", "error": err.Error()})
		return
	}
	if err := userService.ForgotPassword(params.Email); err != nil {
		log.Println(err)
		s, m := ParseForgotPasswordAPIError(err)
		w.WriteHeader(s)
		render.JSON(w, r, services.M{"message": m, "error": err.Error()})
		return
	}
	render.JSON(w, r, services.M{"message": "email sent"})
}

// change password
func changePassword(w http.ResponseWriter, r *http.Request) {
	var params user.ChangePassword
	if err := render.DecodeJSON(r.Body, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "missing or invalid params", "error": err.Error()})
		return
	}
	uid, err := mid.UserIDWithError(r)
	if err != nil {
		w.WriteHeader(401)
		render.JSON(
			w, r,
			services.M{"message": "invalid auth token", "error": "invalid token"})
		return
	}

	params.ID = uid
	oldPass, err := userService.GetPass(params.ID)
	if err != nil {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
		return
	}

	if ok := authService.IsPasswordValid(oldPass, params.OldPassword); !ok {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "unable to valid user", "error": "invalid password"})
		return
	}
	if err := userService.ChangePassword(params); err != nil {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	} else {
		render.JSON(w, r, services.M{"message": "successfully updated"})
	}
}
