package follow

import (
	"log"
	"net/http"
	"strconv"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"
	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/follow"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

type Router interface {
	Followers(http.ResponseWriter, *http.Request)
	Followings(http.ResponseWriter, *http.Request)
	Follow(http.ResponseWriter, *http.Request)
	AcceptRequest(http.ResponseWriter, *http.Request)
	DenyRequest(http.ResponseWriter, *http.Request)
	BlockedUsers(http.ResponseWriter, *http.Request)
	Mount(*chi.Mux)
}

type router struct {
	service follow.Service
}

func New() Router {
	return &router{follow.NewService()}
}

func (route *router) routes() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/followers", route.Followers)
	r.Get("/followings", route.Followings)
	r.Get("/block", route.BlockedUsers)
	r.Post("/block", route.BlockUnblock)
	r.Post("/follow", route.Follow)
	r.Post("/accept-follow", route.AcceptRequest)
	r.Post("/deny-follow", route.DenyRequest)
	return r
}

func (route *router) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Use(mid.Authenticator())
		ro.Use(render.SetContentType(render.ContentTypeJSON))
		ro.Mount("/api/user/{id}", route.routes())
	})
}

// list of the followers
func (route *router) Followers(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r) // retrieved from the jwt
	uid := chi.URLParam(r, "id")
	pg, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	qp := r.URL.Query().Get("query")
	if list, err := route.service.Followers(cuid, uid, qp, pg); err == nil {
		render.JSON(w, r, list)
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// list of the following users
func (route *router) Followings(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r) // retrieved from the jwt
	uid := chi.URLParam(r, "id")
	pg, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	qp := r.URL.Query().Get("query")
	if list, err := route.service.Followings(cuid, uid, qp, pg); err == nil {
		render.JSON(w, r, list)
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// follow/unfollow user
func (route *router) Follow(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r) // retrieved from the jwt
	uid := chi.URLParam(r, "id")
	if cuid == uid {
		render.JSON(w, r, services.M{"message": "can't follow yourself", "error": "found same user id"})
		w.WriteHeader(400)
		return
	}
	var resp follow.FollowResponse
	resp = route.service.Follow(cuid, uid)
	if resp.Error != "" {
		w.WriteHeader(500)
	}
	render.JSON(w, r, resp)
}

// accept the following request
func (route *router) AcceptRequest(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r) // retrieved from the jwt
	uid := chi.URLParam(r, "id")
	if err := route.service.AcceptRequest(cuid, uid); err == nil {
		render.JSON(w, r, services.M{"message": "request accepted"})
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// deny the following request
func (route *router) DenyRequest(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r) // retrieved from the jwt
	uid := chi.URLParam(r, "id")
	if err := route.service.DenyRequest(cuid, uid); err == nil {
		render.JSON(w, r, services.M{"message": "request denied"})
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// list of the blocked users
func (route *router) BlockedUsers(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r) // retrieved from the jwt
	// uid := chi.URLParam(r, "id")
	if users, err := route.service.BlockedUsers(cuid); err == nil {
		render.JSON(w, r, users)
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "error encounter", "error": err.Error()})
	}
}

// block unblock user
func (route *router) BlockUnblock(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r) // retrieved from the jwt
	uid := chi.URLParam(r, "id")
	if resp, err := route.service.BlockUnblock(cuid, uid); err == nil {
		render.JSON(w, r, resp)
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "error encounter", "error": err.Error()})
	}
}
