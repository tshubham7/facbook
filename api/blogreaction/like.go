package blogreaction

import (
	"log"
	"net/http"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/blogreaction"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

// LikeRouter interface that manages routes of like services
type LikeRouter interface {
	Like(http.ResponseWriter, *http.Request)
	DisLike(http.ResponseWriter, *http.Request)
	List(http.ResponseWriter, *http.Request)
	Mount(*chi.Mux)
}

type likeRouter struct {
	service blogreaction.LikeService
}

// NewLikeRouter function to control the like service routes
func NewLikeRouter() LikeRouter {
	return &likeRouter{blogreaction.NewLikeService()}
}

func (rt *likeRouter) routes() chi.Router {
	r := chi.NewRouter()
	r.Post("/", rt.Like)
	r.Delete("/", rt.DisLike)
	r.Get("/", rt.List)
	r.Get("/count", rt.LikeCount)
	return r
}

func (rt *likeRouter) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Use(mid.Authenticator())
		ro.Use(render.SetContentType(render.ContentTypeJSON))
		ro.Mount("/api/blog/{blog-id}/like", rt.routes())
	})
}

// create a like on a blog
func (rt *likeRouter) Like(w http.ResponseWriter, r *http.Request) {
	var params blogreaction.LikeParam
	var err error
	if err = render.Decode(r, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "invalid or missing params", "error": err.Error()})
		return
	}
	params.BlogID = chi.URLParam(r, "blog-id")
	params.UserID = mid.UserID(r)
	var resp LikeResponse
	if resp.Count, resp.Msg, err = rt.service.React(params); err == nil {
		render.JSON(w, r, resp)
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": ValidateLikeError(err), "error": err.Error()})
	}
}

// dislike a blog
func (rt *likeRouter) DisLike(w http.ResponseWriter, r *http.Request) {
	cuid := mid.UserID(r)
	blogID := chi.URLParam(r, "blog-id")
	var err error
	var resp LikeResponse
	if resp.Count, err = rt.service.DisLike(cuid, blogID); err == nil {
		resp.Msg = "blog disliked"
		render.JSON(w, r, resp)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// liked user list
func (rt *likeRouter) List(w http.ResponseWriter, r *http.Request) {
	blogID := chi.URLParam(r, "blog-id")
	if list, err := rt.service.List(blogID); err == nil {
		render.JSON(w, r, list)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// blog like counts
func (rt *likeRouter) LikeCount(w http.ResponseWriter, r *http.Request) {
	blogID := chi.URLParam(r, "blog-id")
	if c, err := rt.service.LikeCount(blogID); err == nil {
		render.JSON(w, r, c)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}
