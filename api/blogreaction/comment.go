package blogreaction

import (
	"log"
	"net/http"

	mid "bitbucket.org/tshubham7/facbook/api/middleware"
	"github.com/go-chi/render"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/blogreaction"
	"github.com/go-chi/chi"
)

// CommentRouter interface for comment api hanler
type CommentRouter interface {
	Create(http.ResponseWriter, *http.Request)
	Update(http.ResponseWriter, *http.Request)
	Delete(http.ResponseWriter, *http.Request)
	List(http.ResponseWriter, *http.Request)
	Mount(*chi.Mux)
}

type commentRouter struct {
	service blogreaction.CommentService
}

// NewCommentRouter manages the handler
func NewCommentRouter() CommentRouter {
	return &commentRouter{blogreaction.NewCommentService()}
}

func (rt *commentRouter) routes() chi.Router {
	r := chi.NewRouter()
	r.Post("/", rt.Create)
	r.Patch("/{id}", rt.Update)
	r.Delete("/{id}", rt.Delete)
	r.Get("/", rt.List)
	r.Post("/{id}/like", rt.Like)
	r.Delete("/{id}/like", rt.Dislike)
	return r
}

func (rt *commentRouter) Mount(r *chi.Mux) {
	r.Group(func(ro chi.Router) {
		ro.Use(mid.Authenticator())
		ro.Use(render.SetContentType(render.ContentTypeJSON))
		ro.Mount("/api/blog/{blog-id}/comment", rt.routes())
	})
}

// create a comment on blog
func (rt *commentRouter) Create(w http.ResponseWriter, r *http.Request) {
	var params blogreaction.CommentParam
	if err := render.Decode(r, &params); err != nil {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "missing or invalid params", "error": err.Error()})
		return
	}
	if params.Content == "" {
		w.WriteHeader(400)
		render.JSON(w, r, services.M{"message": "can not create a blank comment",
			"error": "empty string found in the comment"})
		return
	}
	params.BlogID = chi.URLParam(r, "blog-id")
	params.UserID = mid.UserID(r)
	if c, err := rt.service.Create(params); err == nil {
		render.JSON(w, r, c)
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// update a comment
func (rt *commentRouter) Update(w http.ResponseWriter, r *http.Request) {
	var params blogreaction.CommentParam
	if err := render.Decode(r, &params); err != nil {
		render.JSON(w, r, services.M{"error": err.Error()})
	}
	id := chi.URLParam(r, "id")
	if err := rt.service.Update(id, params); err == nil {
		render.JSON(w, r, services.M{"message": "successfully updated"})
	} else {
		w.WriteHeader(500)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// delete a comment
func (rt *commentRouter) Delete(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if err := rt.service.Delete(id); err == nil {
		render.JSON(w, r, services.M{"message": "successfully deleted"})
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// list of comments
func (rt *commentRouter) List(w http.ResponseWriter, r *http.Request) {
	blogID := chi.URLParam(r, "blog-id")
	cuid := mid.UserID(r)
	if list, err := rt.service.List(cuid, blogID); err == nil {
		render.JSON(w, r, list)
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// like a comment
func (rt *commentRouter) Like(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	blogID := chi.URLParam(r, "blog-id")
	cuid := mid.UserID(r)
	var err error
	if err = rt.service.Like(cuid, id, blogID); err == nil {
		render.JSON(w, r, services.M{"message": "like added"})
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}

// dislike a comment
func (rt *commentRouter) Dislike(w http.ResponseWriter, r *http.Request) {
	var err error
	id := chi.URLParam(r, "id")
	cuid := mid.UserID(r)
	if err = rt.service.Dislike(cuid, id); err == nil {
		render.JSON(w, r, services.M{"message": "like removed"})
	} else {
		w.WriteHeader(500)
		log.Println(err)
		render.JSON(w, r, services.M{"message": "something went wrong", "error": err.Error()})
	}
}
