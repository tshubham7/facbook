package blogreaction

import "strings"

// ValidateLikeError validation for the like api error
func ValidateLikeError(err error) string {
	if strings.Contains(err.Error(), "blog_like_blog_id_fkey") {
		return "invalid blog id"
	}
	return "something went wrong"
}
