package blogreaction

// LikeResponse struct for like response
type LikeResponse struct {
	Count int64  `json:"likeCount"`
	Msg   string `json:"message"`
}
