package validation

import (
	"database/sql"
	"errors"

	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/jmoiron/sqlx"
)

// Service interface implements the service function for validation app
type Service interface {
	Username(currentUserID, username string) error
	Email(currentUserID, email string) error
	PhoneNumber(currentUserID, phoneNumber string) error
}

type store struct {
	*sqlx.DB
}

// New function manages the service function for validation app
func New() Service {
	return &store{db.DBWrite}
}

// check the existance of the email
func (store *store) Username(userID, u string) error {
	var id string
	q := `SELECT id FROM facbook_user WHERE username=$1`
	err := store.Get(&id, q, u)
	if err != nil && err != sql.ErrNoRows {
		return err
	}
	if id != userID {
		return errors.New("already exists in the db")
	}
	return nil
}

// check the existance of the email
func (store *store) Email(userID, e string) error {
	var id string
	q := `SELECT id FROM facbook_user WHERE email=$1`
	err := store.Get(&id, q, e)
	if err != nil && err != sql.ErrNoRows {
		return err
	}
	if id != userID {
		return errors.New("already exists in the db")
	}
	return nil
}

// check the existance of the phone number
func (store *store) PhoneNumber(userID, m string) error {
	var id string
	q := `SELECT id FROM facbook_user 
		WHERE mobile=$1 OR mobile=REPLACE(REPLACE($1, ' ', ''), '-', '')`
	err := store.Get(&id, q, m)
	if err != nil && err != sql.ErrNoRows {
		return err
	}
	if id != userID {
		return errors.New("already exists in the db")
	}
	return nil
}
