package profile

import (
	"fmt"
	"strings"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/db"
	"bitbucket.org/tshubham7/facbook/services/image"
	"github.com/jmoiron/sqlx"
)

// Service interface
type Service interface {
	GetByID(currentUserID string, userID string) (User, error)
	GetByUsername(currentUserID string, username string) (User, error)
	Update(string, Update) (Update, error)
	UpdateAvatar(userID string, base64Image string) (string, error)

	UpdatePhone(userID string, phoneNumber string) error
	VerifyPhone(userID string, phoneNumber string, OTP string) error

	UpdateEmail(userID string, email string) error
	VerifyEmail(userID string, email string, OTP string) error
}

type profileStore struct {
	*sqlx.DB
}

// NewService function to serve the interface
func NewService() Service {
	return &profileStore{db.DBWrite}
}

var profileQuery = `SELECT id, name, username, avatar, bio, is_private,
	(SELECT COUNT(id) FROM blog_view WHERE user_id=u.id) AS blogs,
	(SELECT COUNT(follower_id) FROM follow WHERE following_id=u.id) AS followers_count,
	(SELECT COUNT(following_id) FROM follow WHERE follower_id=u.id) AS following_count,
	(SELECT EXISTS (
		SELECT 1 FROM follow WHERE following_id=u.id AND follower_id=$2)) AS following,
	(SELECT EXISTS (
		SELECT 1 FROM profile_blockeduser WHERE user_id=u.id AND owner_id=$2)) AS is_blocked,
	(SELECT EXISTS (
		SELECT 1 FROM profile_blockeduser WHERE owner_id=u.id AND user_id=$2)) AS are_blocked,
	(SELECT coalesce(
		(SELECT accepted FROM follow WHERE following_id=u.id AND follower_id=$2), false)) AS request_accepted_them,
	(SELECT coalesce(
		(SELECT accepted FROM follow WHERE following_id=$2 AND follower_id=u.id), false)) AS request_accepted_you,
	(SELECT array_to_json(
		ARRAY(
			SELECT avatar_thumbnail FROM facbook_user WHERE id IN (
				SELECT follower_id FROM follow WHERE following_id=u.id LIMIT 2)), true)) AS follower_images,
	(SELECT array_to_json(
		ARRAY(
			SELECT avatar_thumbnail FROM facbook_user WHERE id IN (
				SELECT following_id FROM follow WHERE follower_id=u.id LIMIT 2)), true)) AS following_images

		FROM facbook_user AS u
`

// user profile details
func (store *profileStore) GetByID(cuid, id string) (User, error) {
	var p User
	q := profileQuery + `WHERE id = $1 AND is_active=true`

	err := store.Get(&p, q, id, cuid)
	return p, err
}

// user profile details by username
func (store *profileStore) GetByUsername(cuid, u string) (User, error) {
	var p User
	q := profileQuery + `WHERE username = $1 AND is_active=true`

	err := store.Get(&p, q, u, cuid)
	return p, err
}

// update user profile
func (store *profileStore) Update(uid string, prm Update) (Update, error) {
	keys := services.SQLGenForUpdate(prm)
	if prm.Bio != nil {
		*prm.Bio = strings.TrimSpace(*prm.Bio) // remove unneccessary space
	}
	q := fmt.Sprintf(
		`UPDATE facbook_user SET %s WHERE id='%s' RETURNING name, bio, username, email`,
		keys, uid)
	stmt, err := store.PrepareNamed(q)
	if err != nil {
		return prm, err
	}
	err = stmt.Get(&prm, prm)
	return prm, err
}

// change user profile image
func (store *profileStore) UpdateAvatar(uid, img string) (string, error) {
	key, _, _ := image.New().ImgInfo(img)
	err := store.saveImage(uid, key)
	go image.New().HandleBase64(img, key, "avatar")
	return key, err
}

// save image to database
func (store *profileStore) saveImage(uid, key string) error {
	q := `UPDATE facbook_user SET avatar=$2, avatar_thumbnail=$3 WHERE id = $1`
	_, err := store.Exec(q, uid, key, "thumbnail_"+key)
	return err
}
