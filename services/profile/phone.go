package profile

import (
	"errors"
	"fmt"
	"log"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/mailer"
	"bitbucket.org/tshubham7/facbook/services/twilio"
)

// preparePhoneOtp function saves otp to the database
func (store *profileStore) preparePhoneOtp(uid, otp string) {
	q := `UPDATE user_token SET mobile_otp=$2, mobile_otp_expiry_time=(CURRENT_TIMESTAMP + INTERVAL '15 min')
		WHERE user_id=$1`
	_, err := store.Exec(q, uid, otp)
	if err != nil {
		log.Println("error encountered while saving/preparing otp: ", err.Error())
		return
	}
}

// UpdatePhone checks the existence of the number
// send the otp code to the number
func (store *profileStore) UpdatePhone(id, m string) error {
	if err := store.checkPhone(m); err != nil {
		return err
	}
	otp := services.GetOtp(5)
	go store.preparePhoneOtp(id, otp)
	twilio.SendSms(m, fmt.Sprintf("otp to verify your number: %s", otp))
	return nil
}

// checkPhone checks the existence of the number
func (store *profileStore) checkPhone(mobile string) error {
	q := `SELECT EXISTS (
		SELECT 1 FROM facbook_user WHERE mobile=$1 OR mobile=REPLACE(REPLACE($1, ' ', ''), '-', ''))`
	var status bool
	err := store.Get(&status, q, mobile)
	if err != nil {
		return err
	}
	if status {
		return errors.New("Number_Unvailable: already exists in the database")
	}
	return nil
}

// VerifyPhone verifies the otp and update the phone number
func (store *profileStore) VerifyPhone(uid, m, otp string) error {
	q := `SELECT EXISTS(
		SELECT 1 FROM user_token 
		WHERE user_id=$1 AND mobile_otp=$2 AND mobile_otp_expiry_time > CURRENT_TIMESTAMP)`
	var status bool
	if err := store.Get(&status, q, uid, otp); err != nil {
		return err
	}
	if !status {
		return errors.New("otp expired")
	}
	if err := store.updatePhone(uid, m); err != nil {
		return err
	}
	return nil
}

// update the number
func (store *profileStore) updatePhone(uid, m string) error {
	tx := store.MustBegin()
	q := `SELECT EXISTS (
		SELECT 1 FROM facbook_user WHERE mobile=$1 OR mobile=REPLACE(REPLACE($1, ' ', ''), '-', ''))`
	var status bool
	err := tx.Get(&status, q, m)
	if err != nil {
		return err
	}
	if status {
		return errors.New("Number_Unvailable: already exists in the database")
	}
	q = `UPDATE facbook_user SET mobile=$2, is_mobile_verified=true WHERE id=$1`
	if _, err = tx.Exec(q, uid, m); err != nil {
		return err
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	go mailer.ChangePhoneNumberSuccess(uid) // sending success email
	return nil
}
