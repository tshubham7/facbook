package profile

import (
	"errors"
	"log"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/mailer"
)

// prepareEmailOtp function saves otp to the database
func (store *profileStore) prepareEmailOtp(uid, otp string) {
	q := `UPDATE user_token SET email_otp=$2, email_otp_expiry_time=(CURRENT_TIMESTAMP + INTERVAL '15 min')
		WHERE user_id=$1`
	_, err := store.Exec(q, uid, otp)
	if err != nil {
		log.Println("error encountered while saving/preparing otp: ", err.Error())
		return
	}
}

// UpdateEmail checks the existence of the email
// send the otp code to the email
func (store *profileStore) UpdateEmail(id, email string) error {
	if err := store.checkEmail(id, email); err != nil {
		return err
	}
	otp := services.GetOtp(5)
	go store.prepareEmailOtp(id, otp)
	go mailer.EmailVerificationOTP(id, email, otp)
	return nil
}

// checkEmail checks the existence of the email
// send the otp code to the email
func (store *profileStore) checkEmail(id, email string) error {
	q := `SELECT EXISTS (
		SELECT 1 FROM facbook_user WHERE email=$1)`
	var status bool
	err := store.Get(&status, q, email)
	if err != nil {
		return err
	}
	if status {
		return errors.New("facbook_user_email_key: already exists in the database")
	}
	return nil
}

// VerifyEmail verifies the otp and update the email
func (store *profileStore) VerifyEmail(uid, email, otp string) error {
	q := `SELECT EXISTS(
		SELECT 1 FROM user_token 
		WHERE user_id=$1 AND email_otp=$2 AND email_otp_expiry_time > CURRENT_TIMESTAMP)`
	var status bool
	if err := store.Get(&status, q, uid, otp); err != nil {
		return err
	}
	if !status {
		return errors.New("otp expired")
	}
	if err := store.updateEmail(uid, email); err != nil {
		return err
	}
	return nil
}

// update the email
func (store *profileStore) updateEmail(uid, email string) error {
	// no need to check email existence because user table has an index for it
	q := `UPDATE facbook_user SET email=$2, is_email_verified=true WHERE id=$1`
	_, err := store.Exec(q, uid, email)
	if err != nil {
		return err
	}
	go mailer.ChangeEmailSuccess(uid) // sending success email
	return nil
}
