package profile

import (
	"github.com/jmoiron/sqlx/types"
)

// User struct for select query, user profile get api
type User struct {
	ID       string `json:"id" db:"id"`
	Name     string `json:"name" db:"name"`
	Avatar   string `json:"avatar" db:"avatar"`
	Username string `json:"username" db:"username"`
	Bio      string `json:"bio" db:"bio"`
	Blogs    string `json:"blogs" db:"blogs"`
	Private  bool   `json:"private" db:"is_private"`

	Following               bool `json:"following" db:"following"`
	NotificationOn          bool `json:"notificationOn" db:"notification_on"`
	RequestAcceptedFromThem bool `json:"requestAcceptedFromThem" db:"request_accepted_them"`
	RequestAcceptedFromYou  bool `json:"requestAcceptedFromYou" db:"request_accepted_you"`
	IsBlocked               bool `json:"isBlocked" db:"is_blocked"`
	AreBlocked              bool `json:"areBlocked" db:"are_blocked"`

	Followers      int            `json:"followers" db:"followers_count"`
	Followings     int            `json:"followings" db:"following_count"`
	FollowerImage  types.JSONText `json:"followerImage" db:"follower_images"`
	FollowingImage types.JSONText `json:"followingImage" db:"following_images"`
}

// type Images struct {
// 	Avatar string `json:"avatar" db:"avatar_thumbnail"`
// }

// Params create user Params
type Params struct {
	Bio      string `json:"bio"`
	Name     string `json:"name"`
	Username string `json:"username"`
	Email    string `json:"email"`

	Avatar string `json:"avatar"`
	Mobile string `json:"mobile"`
}

// Update update user Params
type Update struct {
	Bio      *string `json:"bio" db:"bio"`
	Name     *string `json:"name" db:"name"`
	Username *string `json:"username" db:"username"`
	Email    *string `json:"email" db:"email"`
}

// Contact struct for select query, user all contacts / chat feature
type Contact struct {
	ID       string `json:"id" db:"id"`
	Avatar   string `json:"avatar" db:"avatar"`
	Username string `json:"username" db:"username"`
	Name     string `json:"name" db:"name"`
}
