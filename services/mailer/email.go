package mailer

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"text/template"

	"bitbucket.org/tshubham7/facbook/properties"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

var key string

func init() {
	key = os.Getenv("FACBOOK_SENDGRID_KEY")
}

var fromEmail = "no-reply@facbook.io"

// IntroductionEmail send the introduction email when user create an account
func IntroductionEmail(userID string) {
	from := mail.NewEmail("Facbook", fromEmail)
	subject := "Facbook, Learn More"
	user, err := NewService().UserDetail(userID)
	if err != nil {
		log.Println("go routine error while fetching user basic details from mailer package", err.Error())
	}

	to := mail.NewEmail(user.Name, user.Email)
	t, _ := template.ParseFiles("templates/email/learn_more.html")
	var tmpl bytes.Buffer
	t.Execute(&tmpl, user)

	message := mail.NewSingleEmail(from, subject, to, "Welcome", tmpl.String())
	cl := sendgrid.NewSendClient(key)
	cl.Send(message)
}

// ResetPassword send the password reset email
func ResetPassword(email, otp string) {
	var data TemplateData
	user, err := NewService().UserDetailEmail(email)
	if err != nil {
		log.Println("go routine error while fetching user basic details from mailer package", err.Error())
	}
	from := mail.NewEmail("Facbook", fromEmail)
	subject := "Facbook, Reset Password"
	to := mail.NewEmail(user.Name, user.Email)
	t, _ := template.ParseFiles("templates/email/password_reset.html")
	var tmpl bytes.Buffer
	data.OTP = otp
	data.Name = user.Name

	t.Execute(&tmpl, data)

	message := mail.NewSingleEmail(from, subject, to, "Reset Password", tmpl.String())
	cl := sendgrid.NewSendClient(key)
	_, err = cl.Send(message)
	if err != nil {
		log.Println(err)
	}
}

// ResetPasswordSuccess send the password reset success
func ResetPasswordSuccess(userID string) {
	user, err := NewService().UserDetail(userID)
	if err != nil {
		log.Println("go routine error while fetching user basic details from mailer package", err.Error())
	}
	from := mail.NewEmail("Facbook", fromEmail)
	subject := "Facbook, Reset Password"
	to := mail.NewEmail(user.Name, user.Email)
	t, _ := template.ParseFiles("templates/email/password_reset_success.html")
	var tmpl bytes.Buffer
	var data TemplateData
	data.Name = user.Name
	t.Execute(&tmpl, data)

	message := mail.NewSingleEmail(from, subject, to, "OTP", tmpl.String())
	cl := sendgrid.NewSendClient(key)
	cl.Send(message)
}

// BlogReportToAdmin send the blog report acknowledge to admin
func BlogReportToAdmin(reporterID, blogID string) {
	from := mail.NewEmail("Facbook", fromEmail)
	subject := "Blog report alert"
	to := mail.NewEmail("admin", properties.AdminEmail)
	content := fmt.Sprintf("user has reported on some blog,\n reporter id=%s, blog id=%s", reporterID, blogID)
	message := mail.NewSingleEmail(from, subject, to, "Blog report alert", content)
	cl := sendgrid.NewSendClient(key)
	cl.Send(message)
}

// UserReportToAdmin send the user report acknowledge to admin
func UserReportToAdmin(reporterID, userID string) {
	from := mail.NewEmail("Facbook", fromEmail)
	subject := "User report alert"
	to := mail.NewEmail("admin", properties.AdminEmail)
	content := fmt.Sprintf(
		"user has reported on some user, reporter id=%s,\n user id=%s", reporterID, userID)
	message := mail.NewSingleEmail(from, subject, to, "Blog report alert", content)
	cl := sendgrid.NewSendClient(key)
	cl.Send(message)
}

// EmailVerification send the email verification link when user create an account
func EmailVerification(email, token string) {
	user, err := NewService().UserDetailEmail(email)
	if err != nil {
		log.Println("go routine error while fetching user basic details from mailer package", err.Error())
	}
	from := mail.NewEmail("Facbook", fromEmail)
	subject := "Facbook, Welcome to Facbook"
	to := mail.NewEmail(user.Name, email)
	t, _ := template.ParseFiles("templates/email/email_verification.html")
	var tmpl bytes.Buffer
	var data TemplateData
	data.URL = properties.EmailVerifyURL + token
	data.Name = user.Name
	t.Execute(&tmpl, data)

	message := mail.NewSingleEmail(from, subject, to, "Welcome", tmpl.String())
	cl := sendgrid.NewSendClient(key)
	cl.Send(message)
}

// EmailVerificationOTP send the otp code through email for email verification
func EmailVerificationOTP(userID, email, otp string) {
	user, err := NewService().UserDetail(userID)
	if err != nil {
		log.Println("go routine error while fetching user basic details from mailer package", err.Error())
	}
	from := mail.NewEmail("Facbook", fromEmail)
	subject := "Facbook, OTP Code"
	to := mail.NewEmail(user.Name, email)
	t, _ := template.ParseFiles("templates/email/email_verification_otp_code.html")
	var tmpl bytes.Buffer
	var data TemplateData
	data.OTP, data.Name = otp, user.Name
	t.Execute(&tmpl, data)

	message := mail.NewSingleEmail(from, subject, to, "OTP", tmpl.String())
	cl := sendgrid.NewSendClient(key)
	cl.Send(message)
}

// ChangeEmailSuccess send the email for email changed success
func ChangeEmailSuccess(userID string) {
	user, err := NewService().UserDetail(userID)
	if err != nil {
		log.Println("go routine error while fetching user basic details from mailer package", err.Error())
	}
	from := mail.NewEmail("Facbook", fromEmail)
	subject := "Facbook, Change Email"
	to := mail.NewEmail(user.Name, user.Email)
	t, _ := template.ParseFiles("templates/email/email_changed.html")
	var tmpl bytes.Buffer
	var data TemplateData
	data.Name, data.Email = user.Name, user.Email
	t.Execute(&tmpl, data)

	message := mail.NewSingleEmail(from, subject, to, "OTP", tmpl.String())
	cl := sendgrid.NewSendClient(key)
	cl.Send(message)
}

// Send2FAOTP send the otp code through email for 2FA
func Send2FAOTP(userID, otp string) {
	user, err := NewService().UserDetail(userID)
	if err != nil {
		log.Println("go routine error while fetching user basic details from mailer package", err.Error())
	}
	from := mail.NewEmail("Facbook", fromEmail)
	subject := "Facbook, OTP Code"
	to := mail.NewEmail(user.Name, user.Email)
	t, _ := template.ParseFiles("templates/email/2fa_login_verification_code.html")
	var tmpl bytes.Buffer
	var data TemplateData
	data.OTP, data.Name = otp, user.Name
	t.Execute(&tmpl, data)

	message := mail.NewSingleEmail(from, subject, to, "OTP", tmpl.String())
	cl := sendgrid.NewSendClient(key)
	cl.Send(message)
}

// ChangePhoneNumberSuccess send the email for phone number success
func ChangePhoneNumberSuccess(userID string) {
	user, err := NewService().UserDetail(userID)
	if err != nil {
		log.Println("go routine error while fetching user basic details from mailer package", err.Error())
	}
	from := mail.NewEmail("Facbook", fromEmail)
	subject := "Facbook, Phone Number"
	to := mail.NewEmail(user.Name, user.Email)
	t, _ := template.ParseFiles("templates/email/phone_number_changed.html")
	var tmpl bytes.Buffer
	var data TemplateData
	data.Name, data.PhoneNumber = user.Name, user.Mobile
	t.Execute(&tmpl, data)

	message := mail.NewSingleEmail(from, subject, to, "OTP", tmpl.String())
	cl := sendgrid.NewSendClient(key)
	cl.Send(message)
}
