package mailer

import (
	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/jmoiron/sqlx"
)

// Service interface
type Service interface {
	UserDetail(userID string) (User, error)
	UserDetailEmail(email string) (User, error)
}

type profileStore struct {
	*sqlx.DB
}

// NewService function to serve the interface
func NewService() Service {
	return &profileStore{db.DBWrite}
}

// UserDetail service function, return user basic details
func (st *profileStore) UserDetail(userID string) (User, error) {
	var u User
	q := `SELECT id, name, email, mobile FROM facbook_user WHERE id=$1`
	err := st.Get(&u, q, userID)
	return u, err
}

// UserDetailEmail service function, return user basic details
func (st *profileStore) UserDetailEmail(email string) (User, error) {
	var u User
	q := `SELECT id, name, email, mobile FROM facbook_user WHERE email=$1`
	err := st.Get(&u, q, email)
	return u, err
}
