package mailer

// TemplateData struct for carring data for html template
type TemplateData struct {
	Name        string
	Email       string
	OTP         string
	Token       string
	URL         string
	PhoneNumber string
}

// User struct for holding user details
type User struct {
	ID     string `json:"id" db:"id"`
	Name   string `json:"name" db:"name"`
	Email  string `json:"email" db:"email"`
	Mobile string `json:"mobile" db:"mobile"`
}
