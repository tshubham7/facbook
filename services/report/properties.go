package report

// BlogReport table name blog_report
type BlogReport struct {
	BlogID     string `json:"blogId" db:"blog_id"`         //reference to Blog table
	ReporterID string `json:"reporterId" db:"reporter_id"` //reference to User table
	UserID     string `json:"userId" db:"user_id"`         //reference to User table
	// Message    string `json:"message" db:"message"`
	ReportType string `json:"type" db:"type"`
}

// UserReport ...
type UserReport struct {
	ReporterID string `json:"reporterId" db:"reporter_id"` //reference to User table
	UserID     string `json:"userId" db:"user_id"`         //reference to User table
	// Message    string `json:"message" db:"message"`
	ReportType string `json:"type" db:"type"`
}
