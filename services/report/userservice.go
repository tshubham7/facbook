package report

import (
	"fmt"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/jmoiron/sqlx"
)

// UserService ...
type UserService interface {
	Create(UserReport) error
}

type userStore struct {
	*sqlx.DB
}

// NewUserService ...
func NewUserService() UserService {
	return &userStore{db.DBWrite}
}

// create report on user
func (store *userStore) Create(ur UserReport) error {
	var err error
	keys := services.SQLGenInsertKeys(ur)
	values := services.SQLGenInsertValues(ur)
	q := fmt.Sprintf(`INSERT INTO user_report(%s) VALUES(%s)`, keys, values)
	_, err = store.NamedExec(q, ur)
	// send email after to the admin
	return err
}
