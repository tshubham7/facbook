package report

import (
	"fmt"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/jmoiron/sqlx"
)

// BlogService ...
type BlogService interface {
	Create(BlogReport) error
}
type blogStore struct {
	*sqlx.DB
}

// NewBlogService ...
func NewBlogService() BlogService {
	return &blogStore{db.DBWrite}
}

// create a record for blog report and send email to admin
func (store *blogStore) Create(pr BlogReport) error {
	keys := services.SQLGenInsertKeys(pr)
	values := services.SQLGenInsertValues(pr)
	q := fmt.Sprintf(`INSERT INTO blog_report(%s) VALUES(%s)`, keys, values)
	if _, err := store.NamedExec(q, pr); err != nil {
		return err
	}
	// send email after to the admin
	// go mailer.BlogReportToAdmin(pr.UserID, pr.BlogID)
	return nil
}
