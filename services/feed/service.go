package feed

import (
	"bitbucket.org/tshubham7/facbook/services/db"
	"bitbucket.org/tshubham7/facbook/services/utility"
	"github.com/jmoiron/sqlx"
)

// Service interface for feed app services
type Service interface {
	Feed(currentUserID string, pageNumber int) ([]Blog, error)
}

type feedStore struct {
	*sqlx.DB
}

// NewService function for feed app services
func NewService() Service {
	return &feedStore{db.DBWrite}
}

// ListQuery string for select query
var ListQuery = `SELECT b.id, b.loc_name, b.caption, b.scheduled_time,
	b.media_key, b.created_at, b.updated_at,
	u.username, u.id AS user_id, u.avatar,
	(SELECT EXISTS (SELECT 1 FROM follow WHERE follower_id=$1 AND following_id=b.user_id)) AS following,
	(SELECT EXISTS (SELECT 1 FROM blog_like WHERE blog_id=b.id AND user_id=$1 AND is_like=true)) AS liked,
	(SELECT COUNT(*) FROM blog_like WHERE blog_id=b.id) AS likes,
	(SELECT COUNT(*) FROM comment WHERE comment.blog_id=b.id) as comments,
	(SELECT COALESCE(array_to_json(array_agg(row_to_json(t)), true), (SELECT '[]'::json))
		FROM ( 
			SELECT u.id, u.username FROM blog_user AS u JOIN blog_tag AS tag ON tag.user_id=u.id 
			WHERE tag.blog_id=b.id AND u.is_active=true 
		) t) as tagged_users
	FROM blog_view AS b JOIN blog_user AS u ON b.user_id = u.id
`

//blog list for Feeds service
func (store *feedStore) Feed(cuid string, p int) ([]Blog, error) {
	// Query to fetch all the blog of the public user, excluding mutual block users and reported blogs
	offset := utility.Util().GetOffset(15, p) // 12 is the limit
	var list = []Blog{}
	q := ListQuery + ` WHERE  
		b.user_id IN (SELECT following_id FROM follow WHERE follower_id=$1 AND accepted=true)
		AND b.user_id NOT IN (
			SELECT user_id FROM profile_blockeduser WHERE owner_id=$1
		) AND b.user_id NOT IN (
			SELECT user_id FROM profile_blockeduser WHERE owner_id=$1
		) AND b.id NOT IN (
			SELECT blog_id FROM blog_report WHERE reporter_id=$1
		) AND b.is_active=true OR b.user_id=$1
		AND b.user_id NOT IN (
			SELECT id FROM blog_user WHERE is_active=false
		) ORDER BY b.created_at DESC LIMIT 15 OFFSET $2
	`
	err := store.Select(&list, q, cuid, offset)
	return list, err
}
