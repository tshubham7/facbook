package feed

import (
	"time"

	"github.com/jmoiron/sqlx/types"
)

// Blog struct for select query, list view
type Blog struct {
	UserID   string `json:"userId" db:"user_id"`
	Avatar   string `json:"avatar" db:"avatar"`
	Username string `json:"username" db:"username"`

	ID            string         `json:"id" db:"id"`
	LocationName  string         `json:"locationName" db:"loc_name"`
	Caption       string         `json:"caption" db:"caption"`
	Liked         bool           `json:"liked" db:"liked"`
	Following     bool           `json:"following" db:"following"`
	Media         string         `json:"media" db:"media_key"`
	Comments      int            `json:"comments" db:"comments"`
	Likes         int            `json:"likes" db:"likes"`
	CreatedAt     time.Time      `json:"createdAt" db:"created_at"`
	UpdatedAt     time.Time      `json:"updatedAt" db:"updated_at"`
	ScheduledTime *time.Time     `json:"scheduledTime" db:"scheduled_time"`
	TaggedUser    types.JSONText `json:"taggedUser" db:"tagged_users"`
}
