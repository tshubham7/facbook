package notification

import "time"

// Select struct for self notification select query
type Select struct {
	BlogID           int64     `json:"blogId" db:"n.action_object_id"`
	BlogImage        string    `json:"blogImage" db:"n.blog_image_key"`
	Content          string    `json:"content" db:"n.description"`
	Msg              string    `json:"msg" db:"n.verb"`
	CreatedAt        time.Time `json:"created_at" db:"n.created_at"`
	ID               int64     `json:"id" db:"n.id"`
	User             *User     `json:"user" db:"u"`
	NotificationType string    `json:"notification_type" db:"n.type"`
}

// Notifications struct for recieving self notifications
type Notifications struct {
	BlogID           int64     `json:"blogId" db:"action_object_id"`
	BlogImage        string    `json:"blogImage" db:"blog_media_key"`
	Content          string    `json:"content" db:"description"`
	Msg              string    `json:"msg" db:"verb"`
	CreatedAt        time.Time `json:"created_at" db:"created_at"`
	ID               int64     `json:"id" db:"id"`
	User             `json:"user" db:"users"`
	NotificationType string `json:"notificationType" db:"type"`
}

// User user's short detail
type User struct {
	ProfileImage string `json:"profileImage" db:"avatar"`
	UserID       int64  `json:"userId" db:"id"`
	Username     string `json:"username" db:"username"`
	Name         string `json:"name" db:"name"`
	Following    bool   `json:"following" db:"following"`
}

// Save struct for notification insert query
type Save struct {
	UserID      string `json:"" db:"user_id"`
	Public      bool   `json:"" db:"public"`
	Verb        string `json:"" db:"verb"`
	ActorObID   string `json:"" db:"actor_object_id"`
	ActionObjID string `json:"" db:"action_object_id"`
	Description string `json:"" db:"description"`
	Type        string `json:"" db:"type"`
	Image       string `json:"" db:"blog_media_key"`
}

// FollowRequests struct for follow request select query
type FollowRequests struct {
	ID        string `json:"id" db:"id"`
	Verb      string `json:"verb" db:"verb"`
	CreatedAt string `json:"createdAt" db:"created_at"`

	UserID    string `json:"userId" db:"user_id"`
	Avatar    string `json:"avatar" db:"avatar"`
	Username  string `json:"username" db:"username"`
	Name      string `json:"name" db:"name"`
	Following bool   `json:"following" db:"following"`
}
