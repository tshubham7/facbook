package notification

import (
	"fmt"
	"log"

	"bitbucket.org/tshubham7/facbook/services"
)

// create a notification
// this will be used all over the app
func (st *store) Save(n Save) {
	keys := services.SQLGenInsertKeys(n)
	values := services.SQLGenInsertValues(n)
	st.Delete(n.Verb)
	q := fmt.Sprintf(`INSERT INTO notification(%s) VALUES(%s)`, keys, values)
	_, err := st.NamedExec(q, &n)
	if err != nil {
		log.Println("go routine error: error while saving notification: ", err.Error())
	}
}

// delete the same notifications
// this deletes all the same notification before the new one being created
// so that there will be only one notification a kind
func (st *store) Delete(v string) {
	q := `DELETE FROM notification WHERE verb=$1`
	if _, err := st.Exec(q, v); err != nil {
		fmt.Println("go routine error: error encountered while deleting older notifications from DB", err.Error())
	}
}
