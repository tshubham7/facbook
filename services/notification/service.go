package notification

import (
	"fmt"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/db"
	"bitbucket.org/tshubham7/facbook/services/utility"
	"github.com/jmoiron/sqlx"
)

// Service ...
type Service interface {
	List(currentUserID string, pageNumber int) ([]Notifications, error)
	Save(notificationParams Save)
	Delete(verb string)
}

type st struct {
	*sqlx.DB
}

// NewService ...
func NewService() Service {
	return &store{db.DBWrite}
}

// List service function notifications list
func (st *store) List(uid string, p int) ([]Notifications, error) {
	var n = []Notifications{}
	var k Select
	offset := utility.Util().GetOffset(40, p)
	keys := services.SQLGenInsertKeys(k)
	q := `SELECT %s, users.id "users.id", users.avatar "users.avatar", users.username "users.username"
		, users.name "users.name",
		(SELECT EXISTS (
			SELECT 1 FROM follow WHERE following_id=users.id AND follower_id=$1)) AS "users.following"
		FROM notification AS n
		JOIN facbook_user AS users ON n.actor_object_id = users.id
		WHERE user_id=$1 AND n.type!='follow' AND public=false 
		AND users.is_active=true
		ORDER BY n.created_at DESC LIMIT 40 OFFSET $2`
	q = fmt.Sprintf(q, keys)
	err := st.Select(&n, q, uid, offset)
	return n, err
}

// FollowRequests service function, list all the following requests
func (st *store) FollowRequests(uid string) ([]FollowRequests, error) {
	var n = []FollowRequests{}
	q := `SELECT n.id, n.verb, n.created_at, u.username, u.name, u.avatar, u.id as user_id,
		(SELECT EXISTS (SELECT 1 FROM follow WHERE following_id=n.actor_object_id AND follower_id=$1)) AS following,
		FROM notification AS n JOIN facbook_user AS u ON u.id=n.actor_object_id
		WHERE n.type='follow' AND n.user_id=$1
		AND u.is_active=true
		ORDER BY n.created_at DESC`
	err := st.Select(&n, q, uid)
	return n, err
}
