package search

import (
	"bitbucket.org/tshubham7/facbook/services/db"
	"bitbucket.org/tshubham7/facbook/services/utility"
	"github.com/jmoiron/sqlx"
)

// Service ...
type Service interface {
	User(currentUserID string, queryParam string, pageNumber int) ([]User, error)
	Hashtag(queryParam string, pageNumber int) ([]Hashtag, error)
	Location(queryParam string, pageNumber int) ([]Location, error)
}

type searchStore struct {
	*sqlx.DB
}

// NewService ...
func NewService() Service {
	return &searchStore{db.DBWrite}
}

// User service function returns the list of users
func (store *searchStore) User(cuid, qp string, pg int) ([]User, error) {
	offset := utility.Util().GetOffset(25, pg)
	q := `SELECT id, username, name, avatar,
		(SELECT EXISTS(
			SELECT 1 FROM follow WHERE follower_id=$2 and following_id=id)) AS following
		FROM facbook_user  
		WHERE id!=$2 
		AND id NOT IN (SELECT user_id FROM profile_blockeduser WHERE owner_id=$2) 
		AND id NOT IN (SELECT owner_id FROM profile_blockeduser WHERE user_id=$2) 
		AND (username ILIKE '%' || $1 || '%' 
			OR
			name ILIKE '%' || $1 || '%'
		) AND is_active=true
		LIMIT 25 OFFSET $3
	`
	var p = []People{}
	err := store.Select(&p, q, qp, cuid, offset)
	return p, err
}

// Location service function returns the list of blog locations
func (store *searchStore) Location(qp string, pg int) ([]Location, error) {
	// page := utility.GetPageNo(pg)
	offset := utility.Util().GetOffset(25, pg)
	q := `SELECT DISTINCT loc_name, 
		(SELECT blog_count_by_location(loc_name)) AS blog_count FROM blog_view 
		WHERE loc_name ILIKE '%' || $1 || '%' AND loc_name!=''
		LIMIT 25 OFFSET $2
	`
	var locs = []Location{}
	err := store.Select(&locs, q, qp, offset)
	return locs, err
}

// Hashtag service function returns the list of hashtags
func (store *searchStore) Hashtag(qp string, pg int) ([]Hashtag, error) {
	offset := utility.Util().GetOffset(25, pg)
	q := `SELECT tag, 
		(SELECT count(id) FROM blog_view WHERE caption LIKE '%' || tag ||'%') AS blog_count 
		FROM hashtag 
		WHERE tag ILIKE '%' || $1 || '%'
		LIMIt 25 OFFSET $2`
	var tags = []Hashtag{}
	err := store.Select(&tags, q, qp, offset)
	return tags, err
}
