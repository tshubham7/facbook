package search

// User struct for holding list of users
type User struct {
	ID        string `json:"id" db:"id"`
	Name      string `json:"name" db:"name"`
	Username  string `json:"username" db:"username"`
	Avatar    string `json:"avatar" db:"avatar"`
	Following bool   `json:"following" db:"following"`
}

// Location struct for holding list of locations
type Location struct {
	Name  string `json:"locationName" db:"loc_name"`
	Count int    `json:"blogs" db:"blog_count"`
}

// Hashtag struct for holding list of hashtags
type Hashtag struct {
	Tag   string `json:"tag" db:"tag"`
	Count int    `json:"blogs" db:"blog_count"`
}
