package complaint

// hold the content of a complaint
type Create struct {
	Email       string `json:"email" db:"email"`
	Username    string `json:"username" db:"username"`
	Title       string `json:"title" db:"title"`
	Description string `json:"description" db:"description"`
}

// complaint details/list
type Complaint struct {
	ID          string  `json:"id" db:"id"`
	Email       string  `json:"email" db:"email"`
	UserID      *string `json:"user_id" db:"user_id"`
	Username    string  `json:"username" db:"username"`
	Title       string  `json:"title" db:"title"`
	Description string  `json:"description" db:"description"`
	Checked     bool    `json:"checked" db:"checked"`
}
