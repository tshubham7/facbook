package complaint

import (
	"fmt"

	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/jmoiron/sqlx"
)

type Service interface {
	Create(Create) error
	Complaints() ([]Complaint, error)
	Check(string) (Complaint, error)
}

type complaintStore struct {
	*sqlx.DB
}

func NewService() Service {
	return &complaintStore{db.DBWrite}
}

// Create a complaint
func (store *complaintStore) Create(prms Create) error {
	q := `INSERT INTO complaint(username, email, description, title, user_id) 
		VALUES(:username, :email, :description, :title, (
			SELECT id FROM facbook_user WHERE username=:username
		))`
	_, err := store.NamedExec(q, prms)
	return err
}

// Create a complaint
func (store *complaintStore) Complaints() ([]Complaint, error) {
	var c = []Complaint{}
	q := `SELECT * FROM complaint`
	err := store.Select(&c, q)
	return c, err
}

// mark check
func (store *complaintStore) Check(id string) (Complaint, error) {
	var c Complaint
	q := fmt.Sprintf(`UPDATE complaint SET checked=true WHERE id='%s' RETURNING *`, id)
	stmt, err := store.PrepareNamed(q)
	if err != nil {
		return c, err
	}
	err = stmt.Get(&c, c) // this is not accepting nil here, so passed the empty struct
	return c, err
}
