package user

// RegisterParams ...
type RegisterParams struct {
	Username    string `json:"username" db:"username"`
	Name        string `json:"name" db:"name"`
	Email       string `json:"email" db:"email"`
	Password    string `json:"password" db:"password"`
	DeviceToken string `json:"device_token" db:"device_token"`
	Avatar      string `json:"avatar" db:"avatar"`
}

// DetailSelectKeys struct for user detail
type DetailSelectKeys struct {
	ID       string `json:"id" db:"id"`
	Name     string `json:"name" db:"name"`
	Email    string `json:"email" db:"email"`
	Username string `json:"username" db:"username"`
	Mobile   string `json:"mobile" db:"mobile"`
	Bio      string `json:"bio" db:"bio"`
	Private  bool   `json:"private" db:"is_private"`
	Avatar   string `json:"avatar" db:"avatar"`
	Password string `json:"-" db:"password"`
}

// Detail struct for user detail
type Detail struct {
	ID       string `json:"id" db:"id"`
	Name     string `json:"name" db:"name"`
	Email    string `json:"email" db:"email"`
	Username string `json:"username" db:"username"`
	Mobile   string `json:"mobile" db:"mobile"`
	Bio      string `json:"bio" db:"bio"`
	Private  bool   `json:"private" db:"is_private"`
	Avatar   string `json:"avatar" db:"avatar"`
	Password string `json:"-" db:"password"`
}

// LoginParam struct for login api
type LoginParam struct {
	ID          string `json:"id" db:"id"`
	Password    string `json:"password" db:"password"`
	Token       string `json:"token" db:"token"`
	OTP         string `json:"otp" db:"otp"`
	DeviceToken string `json:"deviceToken"`

	Email       string `json:"email" db:"email"`
	PhoneNumber string `json:"mobile" db:"mobile"`
}

// ChangePassword struct for change password API
type ChangePassword struct {
	Password    string `json:"password" db:"password"`
	OldPassword string `json:"oldPassword"`
	ID          string `json:"id" db:"id"`
}
