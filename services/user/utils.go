package user

import "github.com/alexandrevicenzi/unchained"

// encrypt the password into hash
// this uses hashed algorithm used in django
func HashPassword(password string) (string, error) {
	hash, err := unchained.MakePassword(password, unchained.GetRandomString(12), "default")
	return hash, err
}
