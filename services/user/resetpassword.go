package user

import (
	"database/sql"
	"errors"
	"log"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/mailer"
	"bitbucket.org/tshubham7/facbook/services/twilio"
)

// change the password
func (st *userStore) ChangePassword(usr ChangePassword) error {
	usr.Password, _ = HashPassword(usr.Password)
	q := `UPDATE facbook_user SET password=$2 WHERE id=$1`
	_, err := st.Exec(q, usr.ID, usr.Password)
	return err
}

// ForgotPassEmail send password reset link to the email
func (st *userStore) ForgotPassEmail(e string) error {
	var otp string
	var err error
	if otp, err = st.prepareOtp(e); err != nil {
		return err
	}
	go mailer.ResetPassword(e, otp) // sending email
	return nil
}

// ForgotPassPhone send password reset link to the email
func (st *userStore) ForgotPassPhone(p string) error {
	var otp string
	var err error
	if otp, err = st.prepareOtp(p); err != nil {
		return err
	}
	message := `request to reset the password? 
	here is your otp: ` + otp
	go twilio.SendSms(p, message) // sending sms
	return nil
}

// verify password reset otp
func (st *userStore) VerifyResetPassOTP(emailOrPhone, otp string) (string, error) {
	var id string
	q := `SELECT u.id
		FROM facbook_user AS u JOIN user_token AS t ON u.id=t.user_id 
		WHERE t.mobile_otp=$1 AND t.mobile_otp_expiry_time > CURRENT_TIMESTAMP AND
		(u.email=$2 OR u.mobile=$2 
			OR u.mobile=REPLACE(REPLACE($2, ' ', ''), '-', ''))
		`
	if err := st.Get(&id, q, otp, emailOrPhone); err != nil {
		return "", err
	}
	return id, nil
}

// reset the password
func (st *userStore) ResetPass(usr LoginParam) error {
	usr.Password, _ = HashPassword(usr.Password)
	q := `UPDATE facbook_user SET password=$2 WHERE id=$1`
	_, err := st.Exec(q, usr.ID, usr.Password)
	return err
}

// get the user password
func (st *userStore) GetPass(id string) (string, error) {
	var pass string
	q := `SELECT password FROM facbook_user WHERE id=$1`
	err := st.QueryRow(q, id).Scan(&pass)
	return pass, err
}

// verify email confirmation
func (st *userStore) VerifyEmail(uid, tkn string) error {
	var email string
	log.Println(uid, tkn)
	q := `UPDATE facbook_user SET is_email_verified=true WHERE 
		id=(SELECT user_id FROM user_token WHERE user_id=$1 AND email_hash_token=$2)
		RETURNING email`

	err := st.QueryRow(q, uid, tkn).Scan(&email)
	if err != nil && err == sql.ErrNoRows {
		return errors.New("link is expired")
	}
	return err
}

// prepare the otp
// get email or phone number
func (st *userStore) prepareOtp(emailOrPhone string) (string, error) {
	otp := services.GetOtp(5)

	q := `UPDATE user_token SET mobile_otp=$2,  
			mobile_otp_expiry_time=CURRENT_TIMESTAMP + INTERVAL '1 day'
			WHERE user_id=(
			SELECT id FROM facbook_user WHERE email=$1 OR mobile=$1 
			OR mobile=REPLACE(REPLACE($1, ' ', ''), '-', '')) RETURNING user_id`
	var id string
	err := st.QueryRow(q, emailOrPhone, otp).Scan(&id)
	return otp, err
}
