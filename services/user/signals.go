package user

import (
	"log"
	"strings"

	"bitbucket.org/tshubham7/facbook/services/mailer"
	"bitbucket.org/tshubham7/facbook/services/profile"
)

// function process some neccessary operations during user registration
func (st *userStore) blogSavedUserSignal(id string, prms RegisterParams) {
	if prms.Avatar != "" {
		profile.NewService().UpdateAvatar(id, prms.Avatar) // set user profile image
	}
	st.buildDeviceToken(id, prms.DeviceToken) // create a record for user device token
	st.PrepareEmailVerificationMail(id, prms)
	mailer.IntroductionEmail(id) // send introduction email to the user
}

// create a record for user device token table for this user
func (st *userStore) buildDeviceToken(id, token string) {
	q := `INSERT INTO user_devicetoken(user_id, token) VALUES($1, $2)`
	if _, err := st.Exec(q, id, token); err != nil {
		log.Println("go routine error: ", err.Error())
	}
}

// create a record for user token table for this user
func (st *userStore) buildUserToken(id string) string {
	q := `INSERT INTO user_token(user_id, email_hash_token) 
		VALUES($1, ENCODE(DIGEST(gen_random_uuid()::text,'sha512'),'hex')) RETURNING email_hash_token`
	var token string
	if err := st.QueryRow(q, id).Scan(&token); err != nil {
		log.Println("go routine error: ", err.Error())
	}
	return token
}

// create a record for user device token table for this user
func (st *userStore) AddDeviceToken(uid, token string) {
	q := `DO
		$Block$
			BEGIN
				IF EXISTS (SELECT true FROM user_devicetoken 
					WHERE user_id=$1 AND token='$2') 
				THEN
					UPDATE user_devicetoken SET is_active=true
					WHERE user_id=$1 AND token='$2';
				ELSE 
					INSERT INTO user_devicetoken(user_id, token) 
					VALUES($1, '$2');
				END IF;
			END
		$Block$
	;`
	q = strings.ReplaceAll(q, "$1", uid)
	q = strings.ReplaceAll(q, "$2", token)
	if _, err := st.Exec(q); err != nil {
		log.Println("go routine error: ", err.Error())
	}
}

// deactivate this token after log out
func (st *userStore) deactivateDeviceToken(uid, token string) error {
	q := `UPDATE user_devicetoken SET is_active=false WHERE user_id=$1 AND token=$2`
	_, err := st.Exec(q, uid, token)
	return err
}

// create a record for user token table for this user
func (st *userStore) PrepareEmailVerificationMail(uid string, prms RegisterParams) {
	go mailer.EmailVerification(prms.Email, st.buildUserToken(uid)) // send introduction email to the user

}
