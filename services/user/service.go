package user

import (
	"fmt"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/jmoiron/sqlx"
)

type Service interface {
	Register(RegisterParams) (Detail, error)
	EmailLogin(string) (Detail, error)
	PhoneLogin(string) (Detail, error)
	Logout(string, string) error
	ChangePassword(ChangePassword) error
	VerifyResetPassOTP(emailOrPhone, otp string) (string, error)
	ForgotPassEmail(email string) error
	ForgotPassPhone(mobile string) error
	ResetPass(LoginParam) error
	GetPass(userID string) (string, error)
	AddDeviceToken(string, string)
	VerifyEmail(userID, token string) error

	GetPasswordByID(cuid string) (string, error)
	Delete(userID string) error
}

type userStore struct {
	*sqlx.DB
}

func NewService() Service {
	return &userStore{db.DBWrite}
}

// register new user
func (st *userStore) Register(rp RegisterParams) (Detail, error) {
	var usr Detail
	var k DetailSelectKeys
	fetchKeys := services.SQLGenInsertKeys(k)
	rp.Password, _ = HashPassword(rp.Password)
	q := fmt.Sprintf(`INSERT INTO facbook_user(email, password, username, name) 
		VALUES(:email, :password, :username, :name, :referral_code) 
		RETURNING %s`, fetchKeys)
	stmt, err := st.PrepareNamed(q)
	if err != nil {
		return usr, err
	}
	err = stmt.Get(&usr, rp)

	if err == nil {
		go st.blogSavedUserSignal(usr.ID, rp)
		return usr, err
	}
	return usr, err
}

// login user, fetch the login details to match with the entered one
func (st *userStore) EmailLogin(email string) (Detail, error) {
	var usr Detail
	var k DetailSelectKeys
	fetchKeys := services.SQLGenInsertKeys(k)
	q := fmt.Sprintf(`SELECT %s,
		(SELECT coalesce(
			(SELECT mode FROM twofactorauth WHERE is_active=True AND user_id=u.id), '')) AS two_fa_mode
		FROM facbook_user AS u WHERE email=$1`, fetchKeys)
	err := st.Get(&usr, q, email)
	return usr, err
}

// login user, fetch the login details to match with the entered one
func (st *userStore) PhoneLogin(email string) (Detail, error) {
	var usr Detail
	var k DetailSelectKeys
	fetchKeys := services.SQLGenInsertKeys(k)
	q := fmt.Sprintf(`SELECT %s,
		(SELECT coalesce(
			(SELECT mode FROM twofactorauth WHERE is_active=True AND user_id=u.id), '')) AS two_fa_mode
		FROM facbook_user AS u WHERE mobile=$1 OR mobile=REPLACE(REPLACE($1, ' ', ''), '-', '')`, fetchKeys)
	err := st.Get(&usr, q, email)
	return usr, err
}

// logout the user
func (st *userStore) Logout(uid, token string) error {
	err := st.deactivateDeviceToken(uid, token) // deactivating the device token
	return err
}

// GetPasswordByID service fetch password for user
func (st *userStore) GetPasswordByID(cuid string) (string, error) {
	q := `Select password FROM facbook_user WHERE id=$1`
	var p string
	err := st.Get(&p, q, cuid)
	return p, err
}

// Delete service deletes the records for user
func (st *userStore) Delete(cuid string) error {
	q := `DELETE facbook_user FROM WHERE id=$1`
	_, err := st.Exec(q, cuid)
	return err
}
