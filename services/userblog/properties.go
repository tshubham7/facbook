package userblog

import "time"

// Blog contains list of blogs
type Blog struct {
	ID           string    `json:"id" db:"id"`
	LocationName string    `json:"locationName" db:"loc_name"`
	Media        string    `json:"media" db:"media_key"`
	Notifying    bool      `json:"notifying" db:"self_notification"`
	CreatedAt    time.Time `json:"createdAt" db:"created_at"`
	UpdatedAt    time.Time `json:"updatedAt" db:"updated_at"`
}
