package userblog

import (
	"bitbucket.org/tshubham7/facbook/services/db"
	"bitbucket.org/tshubham7/facbook/services/utility"
	"github.com/jmoiron/sqlx"
)

// Service ...
type Service interface {
	Blog(userID string, pageNumber int) ([]Blog, error)
}

type store struct {
	*sqlx.DB
}

// NewService ...
func NewService() Service {
	return &store{db.DBWrite}
}

// Blogs service function returns user's blog list
func (st *store) Blogs(uid string, p int) ([]Blog, error) {
	var list = []Self{}
	offset := utility.Util().GetOffset(20, p)

	q := `SELECT b.id, b.created_at, b.updated_at, loc_name,
		b.media_key
		FROM blog_view AS b JOIN blog_media AS m ON b.id=m.blog_id
		WHERE b.user_id=$1 ORDER BY b.created_at DESC
		LIMIT 20 OFFSET $2	
	`
	err := st.Select(&list, q, uid, offset)
	return list, err
}
