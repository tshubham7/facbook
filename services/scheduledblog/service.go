package scheduledblog

import (
	"fmt"

	"bitbucket.org/tshubham7/facbook/services"
	helper "bitbucket.org/tshubham7/facbook/services/bloghelper"
	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/jmoiron/sqlx"
)

// Service ...
type Service interface {
	List(currentUserID string) ([]ScheduledBlog, error)
	ShareNow(currentUserID, blogID string) error
	Update(currentUserID string, blogID string, updateParams UpdateBlogParam) (UpdatedScheduledBlog, error)
}

type store struct {
	*sqlx.DB
}

// NewService ...
func NewService() Service {
	return &store{db.DBWrite}
}

var feedQuery = `SELECT b.id, b.loc_name, b.caption, b.scheduled_time,
	b.media_key, b.created_at, b.updated_at,
	u.username, u.id AS user_id, u.avatar,
	(SELECT EXISTS (SELECT 1 FROM follow WHERE follower_id=$1 AND following_id=b.user_id)) AS following,
	(SELECT EXISTS (SELECT 1 FROM blog_like WHERE blog_id=b.id AND user_id=$1 AND is_like=true)) AS liked,
	(SELECT COUNT(*) FROM blog_like WHERE blog_id=b.id) AS likes,
	(SELECT COUNT(*) FROM comment WHERE comment.blog_id=b.id) as comments,
	(SELECT COALESCE(array_to_json(array_agg(row_to_json(t)), true), (SELECT '[]'::json))
		FROM ( 
			SELECT u.id, u.username FROM facbook_user AS u JOIN blog_tag AS tag ON tag.user_id=u.id WHERE tag.blog_id=b.id 
		) t) as tagged_users

	FROM blog AS b JOIN facbook_user AS u ON b.user_id = u.id
`

// scheduled blog list for the user
func (st *store) List(cuid string) ([]ScheduledBlog, error) {
	q := feedQuery + ` WHERE b.user_id=$1 AND b.is_active=false AND b.deleted_at IS NULL`
	var p = []ScheduledBlog{}
	err := st.Select(&p, q, cuid)
	return p, err
}

// share the scheduled blog
func (st *store) ShareNow(cuid, id string) error {
	var caption string
	q := `UPDATE blog SET is_active=true, created_at=CURRENT_TIMESTAMP WHERE id=$1 RETURNING caption`
	err := st.QueryRow(q, id).Scan(&caption)
	if err != nil {
		return err
	}
	go helper.GoRoutine().HandleMention(cuid, caption) // send notification to the mentioned users
	go helper.Notify().NotifyTaggedUsers(cuid, id)     // send notification to tagged users
	return nil
}

// update the scheduled blog
func (st *store) Update(cuid, id string, prm UpdateBlogParam) (UpdatedScheduledBlog, error) {
	var up Update
	up.Caption, up.Latitude, up.ScheduledTime = prm.Caption, prm.Latitude, prm.ScheduledTime
	up.Longitude, up.LocationName = prm.Longitude, prm.LocationName
	p, err := st.update(cuid, id, up)
	if err != nil {
		return p, err
	}
	if prm.TaggedUser != nil {
		go helper.GoRoutine().HandleTaggedUsers(cuid, id, *prm.TaggedUser, false) // send notification to tagged users
	}
	go helper.GoRoutine().ExtractHashtag(*prm.Caption) // save the hashtags from the caption
	return p, nil
}

// update the scheduled blog
func (st *store) update(cuid, id string, prm Update) (UpdatedScheduledBlog, error) {
	keys := services.SQLGenForUpdate(prm)
	var p UpdatedScheduledBlog
	q := fmt.Sprintf(`UPDATE blog 
		SET %s, loc_position=ST_SetSRID(ST_MakePoint(:longitude, :latitude), 4326) WHERE id='%s'
		RETURNING caption, scheduled_time, loc_name, latitude, longitude`, keys, id)

	if prm.Latitude == nil {
		q = fmt.Sprintf(
			`UPDATE blog SET %s  WHERE id='%s' RETURNING caption, scheduled_time, loc_name, latitude, longitude`, keys, id)
	}
	stmt, err := st.PrepareNamed(q)
	if err != nil {
		return p, err
	}

	err = stmt.Get(&p, prm)
	return p, err
}
