package push

type DeviceRegister struct {
	ID     *string `json:"id" db:"id"`
	Token  string  `json:"token" db:"token"`
	UserID string  `json:"user_id" db:"user_id"`
}
