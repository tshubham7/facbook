package push

import (
	"fmt"

	"bitbucket.org/tshubham7/facbook/services"
	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/jmoiron/sqlx"
)

//Device ..
type Device interface {
	Register(string, string) error
	GetToken(userID string) (DeviceRegister, error)
}

type device struct {
	*sqlx.DB
}

//New returns a new device service
func New() Device {
	return device{db.DBWrite}
}

func (d device) Register(uid, token string) error {
	register, err := d.GetToken(uid)
	if err == nil && register.Token == token {
		return nil
	}
	var create DeviceRegister
	create.UserID = uid
	create.Token = token
	keys := services.SQLGenInsertKeys(create)
	values := services.SQLGenInsertValues(create)

	query := fmt.Sprintf("INSERT INTO user_devicetoken(%s) VALUES(%s)", keys, values)
	stmt, err := d.PrepareNamed(query)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(create)

	return err
}

func (d device) GetToken(userID string) (DeviceRegister, error) {
	var register DeviceRegister
	err := d.Get(&register, "SELECT id, token, user_id FROM  user_devicetoken WHERE user_id = $1", userID)
	return register, err
}
