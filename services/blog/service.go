package blog

import (
	"fmt"
	"strings"

	"bitbucket.org/tshubham7/facbook/services"
	helper "bitbucket.org/tshubham7/facbook/services/bloghelper"
	"bitbucket.org/tshubham7/facbook/services/db"
	"bitbucket.org/tshubham7/facbook/services/image"
	"github.com/jmoiron/sqlx"
)

// Service interface manages the services for blog
type Service interface {
	Create(BlogParam) (string, error)
	Update(string, UpdateBlogParam) (UpdatedBlog, error)
	TaggedUsers(currentUserID, blogID string) ([]User, error)
	ByID(currentUserID, blogID string) (Detail, error)
	Delete(blogID string) error
}

type blogStore struct {
	*sqlx.DB
}

// NewService manage the service functions for blog
func NewService() Service {
	return &blogStore{db.DBWrite}
}

// Create create a blog
func (store *blogStore) Create(prm BlogParam) (string, error) {
	var id string
	var err error

	key, h, w := image.New().ImgInfo(prm.Media)
	prm.Caption = strings.TrimSpace(prm.Caption) // remove unneccessary space
	if id, err = store.saveBlog(prm, key, h, w); err != nil {
		return "", err
	}

	go store.processBlog(prm, id, key) // process remaining operations in the background
	return id, nil
}

// some blog operations to be processed
func (store *blogStore) processBlog(prm BlogParam, id, key string) {
	if prm.ScheduledTime == nil {
		prm.IsActive = true
	}

	go image.New().HandleBase64(prm.Media, key, "blog")

	if len(prm.TaggedUser) > 0 {
		go helper.GoRoutine().HandleTaggedUsers(prm.UserID, id, prm.TaggedUser, prm.IsActive) // send notification to tagged users
	}
	go helper.GoRoutine().ExtractHashtag(prm.Caption) // save the hashtags from the caption

	if prm.IsActive && prm.Caption != "" {
		go helper.GoRoutine().HandleMention(prm.UserID, prm.Caption) // send notification to the mentioned users
	}
}

// save blog details on the database
func (store *blogStore) saveBlog(prm BlogParam, key string, h, w int) (string, error) {
	var id string
	q := `INSERT INTO blog(
			caption, user_id, scheduled_time, loc_name, longitude, latitude, loc_position, is_active, media_key) 
		VALUES(:caption, :user_id, :scheduled_time, :loc_name, :longitude, :latitude,
			ST_SetSRID(ST_MakePoint(:longitude, :latitude), 4326), :is_active, media_key) 
		RETURNING id`

	stmt, err := store.PrepareNamed(q)
	if err != nil {
		return "", err
	}
	err = stmt.Get(&id, prm)
	return id, err
}

// update a blog
func (store *blogStore) Update(id string, prm UpdateBlogParam) (UpdatedBlog, error) {
	var err error
	var p UpdatedBlog
	var up UpdateBlog
	up.Caption, up.Latitude, up.Longitude, up.LocationName = prm.Caption, prm.Latitude, prm.Longitude, prm.LocationName

	if p, err = store.updateBlog(id, up); err != nil {
		return p, err
	}
	if prm.TaggedUser != nil {
		go helper.GoRoutine().HandleTaggedUsers(prm.UserID, id, *prm.TaggedUser, true) // send notification to tagged users
	}
	go helper.GoRoutine().ExtractHashtag(*prm.Caption)            // save the hashtags from the caption
	go helper.GoRoutine().HandleMention(prm.UserID, *prm.Caption) // send notification to the mentioned users

	return p, nil
}

// update blog details on the database
func (store *blogStore) updateBlog(id string, prm UpdateBlog) (UpdatedBlog, error) {
	keys := services.SQLGenForUpdate(prm)
	var p UpdatedBlog
	q := fmt.Sprintf(`UPDATE blog 
		SET %s, loc_position=ST_SetSRID(ST_MakePoint(:longitude, :latitude), 4326) WHERE id='%s'
		RETURNING caption, loc_name, latitude, longitude`, keys, id)

	if prm.Latitude == nil {
		q = fmt.Sprintf(`UPDATE blog SET %s  WHERE id='%s' RETURNING caption, loc_name, latitude, longitude`, keys, id)
	}

	stmt, err := store.PrepareNamed(q)
	if err != nil {
		return p, err
	}
	err = stmt.Get(&p, prm)
	return p, err
}

// ByID get the blog detail by id
func (store *blogStore) ByID(cuid, id string) (Detail, error) {
	var blog Detail
	var err error

	q := `SELECT b.id, b.loc_name, b.caption, b.scheduled_time,
		b.media_key, b.created_at, b.updated_at,
		u.username, u.id AS user_id, u.avatar,
		(SELECT EXISTS (SELECT 1 FROM blog_like WHERE blog_id=b.id AND user_id=$1 AND is_like=true)) AS liked,
		(SELECT EXISTS (SELECT 1 FROM follow WHERE follower_id=$1 AND following_id=b.user_id)) AS following,
		(SELECT COUNT(*) FROM blog_like WHERE blog_id=b.id) AS likes,
		(SELECT COUNT(*) FROM comment WHERE blog_id=b.id) AS comments,
		(SELECT COALESCE(array_to_json(array_agg(row_to_json(t)), true), (SELECT '[]'::json))
			FROM ( 
				SELECT u.id, u.username FROM facbook_user AS u JOIN blog_tag AS tag ON tag.user_id=u.id 
				WHERE tag.blog_id=b.id AND u.is_active=true
			) t) as tagged_users

		FROM blog AS b JOIN facbook_user AS u ON b.user_id = u.id
		WHERE b.id=$2
	`
	if err = store.Get(&blog, q, cuid, id); err != nil {
		return blog, err
	}
	return blog, err
}

// Delete delete a blog
func (store *blogStore) Delete(blogID string) error {
	q := `DELETE FROM blog WHERE id=$1`
	_, err := store.Exec(q, blogID)
	return err
}

// blog tagged users list
func (store *blogStore) TaggedUsers(cuid, blogID string) ([]User, error) {
	q := `SELECT id, username, name, avatar,
		(SELECT EXISTS(
			SELECT 1 FROM follow WHERE follower_id=$2 and following_id=id)) AS following 
		FROM facbook_user 
		WHERE id IN (SELECT user_id FROM blog_tag WHERE blog_id=$1)
		AND is_active=true
	`
	var p = []User{}
	err := store.Select(&p, q, blogID, cuid)
	return p, err
}
