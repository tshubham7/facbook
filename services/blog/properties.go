package blog

import (
	"time"

	"github.com/jmoiron/sqlx/types"
)

type User struct {
	ID        string `json:"id" db:"id"`
	Name      string `json:"name" db:"name"`
	Username  string `json:"username" db:"username"`
	Avatar    string `json:"avatar" db:"avatar"`
	Following bool   `json:"following" db:"following"`
}

// Detail blog detail, for select query
type Detail struct {
	UserID   string `json:"userId" db:"user_id"`
	Avatar   string `json:"avatar" db:"avatar"`
	Username string `json:"username" db:"username"`

	ID            string     `json:"id" db:"id"`
	LocationName  string     `json:"locationName" db:"loc_name"`
	Caption       string     `json:"caption" db:"caption"`
	Liked         bool       `json:"liked" db:"liked"`
	Following     bool       `json:"following" db:"following"`
	Media         string     `json:"media" db:"media_key"`
	ScheduledTime *time.Time `json:"scheduledTime" db:"scheduled_time"`
	// LikedUserAvatar []Avatars `json:"likedUserAvatar" db:"images"`
	Comments   int             `json:"comments" db:"comments"`
	Likes      int             `json:"likes" db:"likes"`
	CreatedAt  time.Time       `json:"createdAt" db:"created_at"`
	UpdatedAt  time.Time       `json:"updatedAt" db:"updated_at"`
	TaggedUser *types.JSONText `json:"taggedUser" db:"tagged_users"`
}

type BlogParam struct {
	Media         string     `json:"media" db:"media_key"`
	TaggedUser    []string   `json:"taggedUser"`
	LocationName  string     `json:"locationName" db:"loc_name"`
	Latitude      float64    `json:"latitude" db:"latitude"`
	Longitude     float64    `json:"longitude" db:"longitude"`
	Caption       string     `json:"caption" db:"caption"`
	UserID        string     `json:"userID" db:"user_id"`
	IsActive      bool       `json:"isActive" db:"is_active"`
	ScheduledTime *time.Time `json:"scheduledTime" db:"scheduled_time"`
}

type UpdateBlogParam struct {
	LocationName  *string    `json:"locationName" db:"loc_name"`
	Latitude      *float64   `json:"latitude" db:"latitude"`
	TaggedUser    *[]string  `json:"taggedUser"`
	Longitude     *float64   `json:"longitude" db:"longitude"`
	Caption       *string    `json:"caption" db:"caption"`
	UserID        string     `json:"userID" db:"user_id"`
	ScheduledTime *time.Time `json:"scheduledTime" db:"scheduled_time"`
}

type UpdateBlog struct {
	LocationName *string  `json:"locationName" db:"loc_name"`
	Latitude     *float64 `json:"latitude" db:"latitude"`
	Longitude    *float64 `json:"longitude" db:"longitude"`
	Caption      *string  `json:"caption" db:"caption"`
}

type UpdatedBlog struct {
	LocationName string  `json:"locationName" db:"loc_name"`
	Latitude     float64 `json:"latitude" db:"latitude"`
	Longitude    float64 `json:"longitude" db:"longitude"`
	Caption      string  `json:"caption" db:"caption"`
}
