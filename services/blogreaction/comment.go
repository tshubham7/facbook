package blogreaction

import (
	"strings"

	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/jmoiron/sqlx"
)

// CommentService interface for comment service
type CommentService interface {
	Create(commentParams CommentParam) (CommentList, error)
	Update(commentID string, commentParams CommentParam) error
	Delete(commentID string) error
	List(currentUserID string, blogID string) ([]CommentList, error)
	Like(userID string, commentID string, blogID string) error
	Dislike(userID string, commentID string) error
}

type commentStore struct {
	*sqlx.DB
}

// NewCommentService serves the comment services
func NewCommentService() CommentService {
	return &commentStore{db.DBWrite}
}

// create a comment on blog
// returning the comment back
func (store *commentStore) Create(prm CommentParam) (CommentList, error) {
	// tx := store.MustBegin()
	prm.Content = strings.TrimSpace(prm.Content) // remove unneccessary space
	q := `INSERT INTO comment(content, user_id, blog_id) 
		VALUES(:content, :user_id, :blog_id) RETURNING id, user_id, content, created_at,
		(SELECT username FROM facbook_user WHERE id=user_id) as username,
		(SELECT avatar FROM facbook_user WHERE id=user_id) as avatar`
	var c CommentList // single comment
	stmt, err := store.PrepareNamed(q)
	if err != nil {
		return c, err
	}
	err = stmt.Get(&c, prm)
	if err != nil {
		return c, err
	}
	return c, err
}

// update a comment
func (store *commentStore) Update(id string, prm CommentParam) error {
	prm.Content = strings.TrimSpace(prm.Content) // remove unneccessary space
	q := `UPDATE comment SET content=$2 WHERE id=$1`
	_, err := store.Exec(q, id, prm.Content)
	return err
}

// delete a comment
func (store *commentStore) Delete(id string) error {
	q := `DELETE FROM comment WHERE id=$1`
	_, err := store.Exec(q, id)
	return err
}

// list of commentes and users who have made comments on the blog
func (store *commentStore) List(cuid, pid string) ([]CommentList, error) {
	q := `SELECT c.id, c.content, c.created_at, u.username, u.id "user_id", u.avatar,
		(SELECT EXISTS (SELECT 1 FROM comment_like WHERE comment_id=c.id AND user_id=$2)) AS liked
		FROM comment AS c JOIN facbook_user AS u ON c.user_id=u.id
		WHERE c.blog_id=$1 AND u.is_active=true
	`
	var list = []CommentList{}
	err := store.Select(&list, q, pid, cuid)
	return list, err
}

// like a comment
func (store *commentStore) Like(cuid, id, pid string) error {
	q := `INSERT INTO comment_like(user_id, comment_id) VALUES($1, $2)
		ON CONFLICT("user_id", "comment_id") DO UPDATE SET user_id=EXCLUDED.user_id`
	_, err := store.Exec(q, cuid, id)
	if err == nil {
		go notify().commentLike(cuid, pid)
	}
	return err
}

// dislike a comment
func (store *commentStore) Dislike(cuid, id string) error {
	q := `DELETE FROM comment_like WHERE user_id=$1 AND comment_id=$2`
	_, err := store.Exec(q, cuid, id)
	return err
}
