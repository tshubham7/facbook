package blogreaction

import "time"

// CommentList is for listing the comment response
type CommentList struct {
	ID        string    `json:"id" db:"id"`
	Content   string    `json:"content" db:"content"`
	Liked     bool      `json:"liked" db:"liked"`
	Username  string    `json:"username" db:"username"`
	Avatar    string    `json:"avatar" db:"avatar"`
	UserID    string    `json:"userID" db:"user_id"`
	CreatedAt time.Time `json:"createdAt" db:"created_at"`
}

// LikeCount for blog
type LikeCount struct {
	Count int `json:"likes" db:"likes"`
}

// LikedUser users who liked the blog
type LikedUser struct {
	ID       string `json:"id" db:"id"`
	Username string `json:"username" db:"username"`
	Avatar   string `json:"avatar" db:"avatar"`
}

// CommentParam for comment create
type CommentParam struct {
	UserID  string `json:"userId" db:"user_id"`
	BlogID  string `json:"blogId" db:"blog_id"`
	Content string `json:"content" db:"content"`
}

// LikeParam for like create
type LikeParam struct {
	Action Action `json:"action"`
	UserID string `json:"userId"`
	BlogID string `json:"blogId"`
}
