package blogreaction

import (
	"log"

	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/jmoiron/sqlx"
)

// LikeService Interface serves like services
type LikeService interface {
	React(likeParams LikeParam) (int64, string, error)
	DisLike(currentUserID string, blogID string) (int64, error)
	List(blogID string) ([]LikedUser, error)
	LikeCount(blogID string) (LikeCount, error)
}

type likeStore struct {
	*sqlx.DB
}

// NewLikeService serves like services
func NewLikeService() LikeService {
	return &likeStore{db.DBWrite}
}

// like the blog
func (store *likeStore) React(param LikeParam) (int64, string, error) {
	var msg string
	var err error
	var count int64

	msg, err = store.like(param.UserID, param.blogID)
	if err != nil {
		return count, msg, err
	}
	q := `SELECT COUNT(*) FROM blog_like WHERE blog_id=$1`
	err = store.QueryRow(q, param.blogID).Scan(&count)
	return count, msg, err
}

// like the blog
func (store *likeStore) like(cuid, blogID string) (string, error) {
	q := `INSERT INTO blog_like(user_id, blog_id) VALUES($1, $2)`
	_, err := store.Exec(q, cuid, blogID)
	go notify().like(cuid, blogID)
	return "like added", err
}

// DisLike dislike the blog
func (store *likeStore) DisLike(cuid, blogID string) (int64, error) {
	var count int64
	q := `SELECT COUNT(*) FROM blog_like WHERE blog_id=$1`
	err := store.QueryRow(q, blogID).Scan(&count)
	if err != nil {
		return 0, err
	}
	go store.deleteLike(cuid, blogID)
	if count == 0 {
		return 0, nil
	}
	return count - 1, nil
}

// deleteLike deleting record for like
func (store *likeStore) deleteLike(cuid, blogID string) {
	q := `DELETE FROM blog_like WHERE user_id=$1 AND blog_id=$2;`
	if err := store.Exec(q, cuid, blogID); err != nil {
		log.Printf("error encountered while deleting like record: %v", err)
	}
}

// list of liked users
func (store *likeStore) List(blogID string) ([]LikedUser, error) {
	var users = []LikedUser{}
	q := `SELECT u.id, u.username, u.avatar
		FROM facbook_user AS u JOIN blog_like AS l ON l.user_id=u.id 
		WHERE l.blog_id=$1 AND u.is_active=true`
	err := store.Select(&users, q, blogID)
	return users, err
}

// blog like counts
func (store *likeStore) LikeCount(blogID string) (LikeCount, error) {
	var c LikeCount
	q := `SELECT COUNT(*) AS likes FROM blog_like WHERE blog_id=$1`
	err := store.Get(&c, q, blogID)
	return c, err
}
