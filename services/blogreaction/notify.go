package blogreaction

import (
	"fmt"
	"log"

	"bitbucket.org/tshubham7/facbook/pushnotification"
	"bitbucket.org/tshubham7/facbook/services/db"
	"bitbucket.org/tshubham7/facbook/services/notification"
	"github.com/jmoiron/sqlx"
)

// Notify interface for the push notifications
type Notify interface {
	comment(actorUserID string, blogID string)
	commentLike(actorUserID string, blogID string)
	like(actorUserID string, blogID string, likeType string)
}

type notifyStore struct {
	*sqlx.DB
}

func notify() Notify {
	return &notifyStore{db.DBWrite}
}

// notify to the blog user when comment is created
func (store *notifyStore) comment(cuid, blogID string) {
	var u1, u2, uid, img string
	var data = notification.NotificationSave{Type: "comment"}
	q := `SELECT (SELECT username FROM facbook_user WHERE id=$1) AS u1,
		(SELECT username FROM facbook_user WHERE id=(
			SELECT user_id FROM blog WHERE id=$2)) AS u2,
		(SELECT user_id FROM blog WHERE id=$2) AS uid,
		(SELECT media_key FROM blog WHERE blog_id=$2) AS img
	`
	err := store.QueryRow(q, cuid, blogID).Scan(&u1, &u2, &uid, &img)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	data.Verb = fmt.Sprintf(`≠%s commented on my blog`, u1)
	// send push notification to the device
	pushnotification.PushToUser(uid, data.Verb)
	data.UserID = uid
	data.ActorObID = cuid
	data.ActionObjID = blogID
	data.Public = false
	data.Image = img
	notification.NewService().Save(data)
	data.Verb = fmt.Sprintf(`≠%s commented on ≠%s 's blog`, u1, u2)
	data.Public = true
	notification.NewService().Save(data)

	// send push notification to users who has turned on notification for this blog
	store.notifyToblogRecipient(cuid, blogID, data.Verb)
}

// notify to the blog user when a like on comment is created
func (store *notifyStore) commentLike(cuid, blogID string) {
	var u1, u2, uid, img string
	var data = notification.NotificationSave{Type: "like"}
	q := `SELECT (SELECT username FROM facbook_user WHERE id=$1) AS u1,
		(SELECT user_id FROM blog WHERE id=$2) AS uid,
		(SELECT media_key FROM blog WHERE blog_id=$2) AS img
	`
	err := store.QueryRow(q, cuid, blogID).Scan(&u1, &uid, &img)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	data.Verb = fmt.Sprintf(`≠%s liked my comment`, u1)
	pushnotification.PushToUser(uid, data.Verb) // send push notification
	data.UserID = uid
	data.ActorObID = cuid
	data.ActionObjID = blogID
	data.Image = img
	notification.NewService().Save(data)
}

// send notification to the user for accepting the following request
//kind is either liked or super liked
func (store *notifyStore) like(cuid, blogID string) {
	var u1, uid, img string
	var data = notification.NotificationSave{Type: "like"}
	q := `SELECT (SELECT username FROM facbook_user WHERE id=$1) AS u1,
		(SELECT user_id FROM blog WHERE id=$2) AS uid,
		(SELECT thumbnail_key FROM blog_media WHERE blog_id=$2) AS img
	`
	err := store.QueryRow(q, cuid, blogID).Scan(&u1, &uid, &img)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	data.Verb = fmt.Sprintf(`≠%s liked my blog`, u1)
	pushnotification.PushToUser(uid, data.Verb) // send notification to blog owner
	data.UserID = uid
	data.ActorObID = cuid
	data.ActionObjID = blogID
	data.Public = false
	data.Image = img
	notification.NewService().Save(data)
}
