package twilio

import (
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
)

var (
	accountSid   = os.Getenv("FACBOOK_ACCOUNT_SID")
	authToken    = os.Getenv("FACBOOK_AUTH_TOKEN")
	twilioNumber = os.Getenv("FACBOOK_TWILIO_NUMBER")
)

var urlStr string

func init() {
	urlStr = "https://api.twilio.com/2010-04-01/Accounts/" + accountSid + "/Messages.json"
}

// SendSms send message to the phone number through twilio
func SendSms(phone, message string) {
	// Build out the data for our message
	v := url.Values{}
	v.Set("To", phone)
	v.Set("From", twilioNumber)
	v.Set("Body", message)
	rb := *strings.NewReader(v.Encode())

	// Create client
	client := &http.Client{}

	req, err := http.NewRequest("POST", urlStr, &rb)
	if err != nil {
		log.Println("twilio error encountered while send sms :", err)
	}
	req.SetBasicAuth(accountSid, authToken)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	// Make request
	if _, err = client.Do(req); err != nil {
		log.Println("twilio error encountered while send sms:", err)
	}
}
