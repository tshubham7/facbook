package db

import (
	"fmt"
	"log"
	"os"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // Only for init
)

var DBWrite *sqlx.DB

func init() {

	writeEndpoint := os.Getenv("FACBOOK_WRITE_END_POINT")
	writePort := os.Getenv("FACBOOK_WRITE_PORT")
	user := os.Getenv("FACBOOK_USER")
	dbName := os.Getenv("FACBOOK_DBNAME")
	password := os.Getenv("FACBOOK_PASSWORD")

	ssl := "require"

	var writeConnection = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		writeEndpoint, writePort, user, password, dbName, ssl,
	)
	var err error
	DBWrite, err = sqlx.Open("postgres", writeConnection)
	if err != nil {
		log.Println(err)
		log.Panic(err)
	}
	err = DBWrite.Ping()
	if err != nil {
		log.Println(err)
		log.Panic(err)
	}
}
