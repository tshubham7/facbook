package auth

import (
	"time"

	"github.com/alexandrevicenzi/unchained"
	"github.com/dgrijalva/jwt-go"

	"github.com/go-chi/jwtauth"
)

const secret = ""

type Authtoken struct {
	Token        string    `json:"token"`
	Expires      time.Time `json:"expires"`
	RefreshToken string    `json:"refreshToken"`
}

// AuthService is the interface that represents the auth service
type AuthService interface {
	TokenAuth() *jwtauth.JWTAuth
	Encode(ID, email string) Authtoken
	IsPasswordValid(has, password string) bool
}

type auth struct {
}

//New creates an Auth Services
func New() AuthService {
	return &auth{}
}

//TokenAuth is the jwt token signed with a secret
func (as *auth) TokenAuth() *jwtauth.JWTAuth {
	return jwtauth.New("HS256", []byte(secret), nil)
}

// // Encode  creates a token with 3 claims
// func (as *auth) Encode(ID, email string) Authtoken {
// 	claims := jwtauth.Claims{}
// 	claims["id"] = ID
// 	if email != "" {
// 		claims["email"] = email
// 	}
// 	claims.SetIssuedNow()
// 	expires := time.Now().Add(time.Hour * time.Duration(24*360))
// 	claims.SetExpiry(expires)
// 	_, tokenString, _ := as.TokenAuth().Encode(claims)
// 	return Authtoken{
// 		Token:   tokenString,
// 		Expires: expires,
// 	}
// }

func (as *auth) Encode(ID, email string) Authtoken {
	expires := time.Now().Add(time.Hour * time.Duration(24*360))
	claims := jwt.MapClaims{}
	claims["id"] = ID
	if email != "" {
		claims["email"] = email
	}
	claims["exp"] = jwtauth.ExpireIn(time.Hour * time.Duration(24*360))
	_, tokenString, _ := as.TokenAuth().Encode(claims)
	return Authtoken{
		Token:   tokenString,
		Expires: expires,
	}
}

// https://github.com/alexandrevicenzi/unchained

// IsPasswrodValid checks if the password is valid
// for a given user by comparing the hash and text password
func (as *auth) IsPasswordValid(salted, password string) bool {
	if valid, _ := unchained.CheckPassword(password, salted); valid {
		return true
	}
	return false
}
