package image

import (
	"bytes"
	"encoding/base64"
	"image"
	"image/jpeg"
	"image/png"
	"log"
	"net/http"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/google/uuid"
)

// s3 bucket name
var bucket = "image.facbook.io"

var ses *session.Session

// init method will intialize the session
func init() {
	// sess, err := session.NewSession(&aws.Config{Region: aws.String("us-west-2")})
	// if err != nil {
	// 	log.Println("failed to create session,", err)
	// }

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config:  aws.Config{Region: aws.String("us-west-2")},
		Profile: "facbook",
	}))
	ses = sess
}

// Uploader ...
type Uploader interface {
	ImgInfo(base64Image string) (key string, height int, width int)
	HandleBase64(base64Image, imageKey, imageType string)

	withJPG(imageObject image.Image, imageKey string)
	withPNG(imageObject image.Image, imageKey string)

	TrimExtraContent(base64Image string) string

	S3upload(s3BucketSession *session.Session, imageKey string, imageBuffer *bytes.Buffer)
}

type uploader struct {
}

// New ...
func New() Uploader {
	return &uploader{}
}

// will get you the image dimension and the key to store in the database
func (up *uploader) ImgInfo(data string) (string, int, int) {
	data = up.TrimExtraContent(data)
	r := base64.NewDecoder(base64.StdEncoding, strings.NewReader(data))
	conf, format, _ := image.DecodeConfig(r)
	id, _ := uuid.NewRandom()
	key := id.String() + "." + format
	return key, conf.Height, conf.Width
}

// handle the base64 image like decode configuration and byte conversion
func (up *uploader) HandleBase64(data, key, imgType string) {
	data = up.TrimExtraContent(data)
	r := base64.NewDecoder(base64.StdEncoding, strings.NewReader(data))
	r2 := base64.NewDecoder(base64.StdEncoding, strings.NewReader(data))
	_, format, err := image.DecodeConfig(r2)
	if err != nil {
		log.Println("error while calculating config", err)
	}

	if format == "jpeg" {
		srcImage, _ := jpeg.Decode(r)
		up.withJPG(srcImage, key)
	}
	if format == "png" {
		srcImage, _ := png.Decode(r)
		up.withPNG(srcImage, key)
	}
}

// encode the jpg image
func (up *uploader) withJPG(img image.Image, key string) {
	buf := new(bytes.Buffer)
	jpeg.Encode(buf, img, nil)
	up.S3upload(ses, key, buf)
}

// encode the png image
func (up *uploader) withPNG(img image.Image, key string) {
	buf := new(bytes.Buffer)
	png.Encode(buf, img)
	up.S3upload(ses, key, buf)
}

// S3upload finally this method will upload image buffer to the s3 method
func (up *uploader) S3upload(s *session.Session, key string, buff *bytes.Buffer) {
	params := &s3.PutObjectInput{
		Bucket:      aws.String(bucket),
		Key:         aws.String(key),
		Body:        bytes.NewReader(buff.Bytes()),
		ContentType: aws.String(http.DetectContentType(buff.Bytes())),
	}
	svc := s3.New(s)
	_, err := svc.PutObject(params)
	if err != nil {
		log.Println("error while updloading on the s3:", err)
	}
}

// trim the meta data from the base64 string
func (up *uploader) TrimExtraContent(image string) string {
	image = strings.NewReplacer("data:image/jpeg;base64,", "").Replace(image)
	return strings.NewReplacer("data:image/png;base64,", "").Replace(image)
}
