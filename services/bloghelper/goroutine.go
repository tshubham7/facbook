package bloghelper

import (
	"fmt"
	"log"
	"strings"

	"bitbucket.org/tshubham7/facbook/pushnotification"
	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/jmoiron/sqlx"
)

type Routine interface {
	HandleTaggedUsers(uid, blogID string, taggedUserId []string, sendPush bool)
	ExtractHashtag(caption string)
	HandleMention(uid, caption string)
}

type utilStore struct {
	*sqlx.DB
}

func GoRoutine() Routine {
	return &utilStore{db.DBWrite}
}

//send blog notifications to tagged users
func (store *utilStore) HandleTaggedUsers(uid, id string, tagId []string, notification bool) {
	q := `DELETE FROM blog_tag WHERE blog_id=$1`
	store.Exec(q, id)
	username, err := Notify().getUsername(uid)
	if err != nil {
		return
	}
	q = `INSERT INTO blog_tag(blog_id, user_id) VALUES($1, $2) `
	msg := fmt.Sprintf("%s tagged me in a blog", username)
	for _, uid := range tagId {
		_, err = store.Exec(q, id, uid)
		if err != nil {
			log.Println("go routine error: ", err)
			continue
		}
		if notification {
			go pushnotification.PushToUser(uid, msg)
		}
	}
}

// extract the hashtag and save it
func (store *utilStore) ExtractHashtag(caption string) {
	q := `INSERT INTO hashtag (tag) 
		SELECT * FROM (SELECT $1) AS tmp
		WHERE NOT EXISTS (
			SELECT tag FROM hashtag WHERE tag = $1
		) LIMIT 1`

	var hashtag string
	words := strings.SplitAfter(caption, " ")
	for _, tag := range words {
		if strings.HasPrefix(tag, "#") {
			hashtag = strings.TrimPrefix(tag, "#")
			store.Exec(q, hashtag)
		}
	}
}

// extract the mentioned users and send then push notifications
func (store *utilStore) HandleMention(uid, caption string) {
	username, err := Notify().getUsername(uid)
	if err != nil {
		return
	}
	msg := fmt.Sprintf("%s mentioned you in a blog", username)
	words := strings.SplitAfter(caption, " ")
	usernames := ""
	r := strings.NewReplacer(" ", "")
	for _, u := range words {
		if strings.HasPrefix(u, "@") {
			usernames += "'" + r.Replace(strings.TrimPrefix(u, "@")) + "', "
		}
	}
	if len(usernames) == 0 {
		return
	}
	usernames = usernames[0 : len(usernames)-2]
	q := fmt.Sprintf("select id from facbook_user where username in (%s)", usernames)
	rows, _ := store.Query(q)
	var id string
	for rows.Next() {
		rows.Scan(&id)
		pushnotification.PushToUser(id, msg)
	}
}
