package bloghelper

import (
	"fmt"

	"bitbucket.org/tshubham7/facbook/pushnotification"
	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/jmoiron/sqlx"
)

type Push interface {
	getUsername(userID string) (string, error)
	NotifyTaggedUsers(UserID string, blogID string)
}

type notifyStore struct {
	*sqlx.DB
}

func Notify() Push {
	return &notifyStore{db.DBWrite}
}

// get the username of the user who blogged the blog
func (store *notifyStore) getUsername(uid string) (string, error) {
	var username string
	q := `SELECT username FROM facbook_user WHERE id=$1`
	err := store.QueryRow(q, uid).Scan(&username)
	return username, err
}

// send blog notifications to tagged users
func (store *notifyStore) NotifyTaggedUsers(uid, pid string) {
	username, err := store.getUsername(uid)
	msg := fmt.Sprintf("%s tagged me in a blog", username)
	q := `SELECT user_id FROM blog_tag WHERE blog_id=$1`
	rows, err := store.Query(q, pid)
	if err != nil {
		return
	}
	var id string
	for rows.Next() {
		rows.Scan(&id)
		pushnotification.PushToUser(id, msg)
	}
}
