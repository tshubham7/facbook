package utility

import (
	"log"

	"bitbucket.org/tshubham7/facbook/services/db"
	"github.com/jmoiron/sqlx"
)

// Service ...
type Service interface {
	GetUsername(userID string) string
	GetName(email string) string
}

type store struct {
	*sqlx.DB
}

// New ...
func New() Service {
	return &store{db.DBWrite}
}

// GetUsername return the username of the user by id
func (st *store) GetUsername(id string) string {
	var username string
	q := `SELECT username FROM facbook_user WHERE id=$1`
	if err := st.QueryRow(q, id).Scan(&username); err != nil {
		log.Println("go routine error while fetching username from email.go file: ", err.Error())
	}
	return username
}

// GetName return the name of the user by id
func (st *store) GetName(e string) string {
	var name string
	q := `SELECT name FROM facbook_user WHERE email=$1`
	if err := st.QueryRow(q, e).Scan(&name); err != nil {
		log.Println("go routine error while fetching name from email.go file ", err.Error())
	}
	return name

}
