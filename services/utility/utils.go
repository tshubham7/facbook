package utility

import (
	"strconv"
)

type Utility interface {
	GetOffset(limit int, pageNumber int) (offset int)
	GetPageNo(pageNumber string) (pgNumber int)
}

type utils struct {
}

func Util() Utility {
	return &utils{}
}

// gives the offset value for the sql query
func (ut *utils) GetOffset(lmt, p int) int {
	var ofs = 0
	if p != 0 {
		ofs = lmt * (p - 1)
	}
	return ofs
}

// this manage the page number, check the proper value of it and give back an integer value
func (ut *utils) GetPageNo(pg string) int {
	// var pageNo int
	pageNo := 0
	if pg != "" {
		pageNo, _ = strconv.Atoi(pg)
	}
	return pageNo
}
