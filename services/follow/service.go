package follow

import (
	"log"

	"bitbucket.org/tshubham7/facbook/services/db"
	"bitbucket.org/tshubham7/facbook/services/utility"
	"github.com/jmoiron/sqlx"
)

// Service ...
type Service interface {
	Followers(
		currentUserID string, userID string,
		queryParam string, pageNumber int) ([]Follower, error)
	Followings(
		currentUserID string, userID string,
		queryParam string, pageNumber int) ([]Follower, error)
	Follow(currentUserID string, userID string) FollowResponse
	AcceptRequest(currentUserID string, userID string) error
	DenyRequest(currentUserID string, userID string) error
	BlockUnblock(currentUserID string, userID string) (BlockUserResponse, error)
	BlockedUsers(currentUserID string) ([]Follower, error)
}

type followStore struct {
	*sqlx.DB
}

// NewService ...
func NewService() Service {
	return &followStore{db.DBWrite}
}

// Followers list of user followers
func (store *followStore) Followers(cuid, uid, qp string, pg int) ([]Follower, error) {
	var list = []Follower{}
	offset := utility.Util().GetOffset(15, pg)
	log.Println(qp, pg)
	q := `SELECT id, username, name, avatar,
		(SELECT EXISTS(
			SELECT 1 FROM follow WHERE follower_id=$2 and following_id=id)) AS following
		FROM facbook_user WHERE id IN (SELECT follower_id FROM follow WHERE following_id=$1)
		AND (
			username ILIKE '%' || $3 || '%' OR
			name ILIKE '%' || $3 || '%'
			) 
		AND is_active=true 
		ORDER BY username
		LIMIT 15 OFFSET $4
	`
	err := store.Select(&list, q, uid, cuid, qp, offset)
	return list, err
}

// list of following users
func (store *followStore) Followings(cuid, uid, qp string, pg int) ([]Follower, error) {
	var data = []Follower{}
	offset := utility.Util().GetOffset(15, pg)
	q := `SELECT id, username, name, avatar,
		(SELECT EXISTS(
			SELECT 1 FROM follow WHERE follower_id=$2 and following_id=id)) AS following
		FROM facbook_user WHERE id IN (SELECT following_id FROM follow WHERE follower_id=$1)
		AND (
			username ILIKE '%' || $3 || '%' OR
			name ILIKE '%' || $3 || '%'
			) 
		AND is_active=true
		ORDER BY username
		LIMIT 15 OFFSET $4
	`
	err := store.Select(&data, q, uid, cuid, qp, offset)
	return data, err
}

// follow unfollow user
func (store *followStore) Follow(cuid, uid string) FollowResponse {
	// fmt.Println(uid)
	resp := store.followUnfollow(cuid, uid)
	return resp
}

// follow unfollow user
func (store *followStore) followUnfollow(cuid, uid string) FollowResponse {
	tx := store.MustBegin()
	var status bool
	var resp FollowResponse
	var q string
	var err error
	// check if user has blocked you or not
	q = `SELECT EXISTS (SELECT 1 FROM profile_blockeduser 
		WHERE owner_id=$2 AND user_id=$1)
	`
	if err = tx.QueryRow(q, cuid, uid).Scan(&status); err != nil {
		resp.Message = "something went wrong"
		resp.Error = err.Error()
		return resp
	}

	if status {
		resp.Message = "you are blocked by this user"
		resp.Error = resp.Message
		return resp
	}
	// checking if already following or not
	q = `SELECT EXISTS (
			SELECT 1 FROM follow WHERE follower_id=$1 AND following_id=$2)
		`
	if err = tx.QueryRow(q, cuid, uid).Scan(&status); err != nil {
		resp.Error = err.Error()
		resp.Message = "something went wrong"
		return resp
	}
	if status {
		q = "DELETE FROM follow WHERE follower_id=$1 AND following_id=$2"
		if _, err = tx.Exec(q, cuid, uid); err != nil {
			resp.Error = err.Error()
			resp.Message = "something went wrong"
			return resp
		}
		resp.Message = "unfollowed successfully"
		resp.IsFollowing = false
	} else {
		q = `INSERT INTO follow(follower_id, following_id, accepted) 
				VALUES($1, $2, (
					SELECT NOT is_private FROM facbook_user WHERE id = $2)) Returning accepted`
		if err = tx.QueryRow(q, cuid, uid).Scan(&resp.RequestApprovedByThem); err != nil {
			resp.Error = err.Error()
			resp.Message = "something went wrong"
			return resp
		}
		if err == nil {
			go notify().follow(cuid, uid, resp.RequestApprovedByThem)
		}
		resp.Message = "followed successfully"
		resp.IsFollowing = true
	}
	tx.Commit()
	return resp
}

// accept the follow request
func (store *followStore) AcceptRequest(cuid, uid string) error {
	q := `UPDATE follow SET accepted=true WHERE following_id=$1 AND follower_id=$2 `
	_, err := store.Exec(q, cuid, uid)
	if err == nil {
		go notify().requestAccept(cuid, uid)
		go store.deleteNotification(cuid, uid, false)
	}
	return err
}

// deny follow request
func (store *followStore) DenyRequest(cuid, uid string) error {
	q := `DELETE FROM follow WHERE following_id=$1 AND follower_id=$2`
	if _, err := store.Exec(q, cuid, uid); err != nil {
		return err
	}
	go store.deleteNotification(cuid, uid, true)
	return nil
}

// delete the notification
func (store *followStore) deleteNotification(cuid, uid string, both bool) {
	q := `DELETE FROM notification 
		WHERE user_id=$1 AND action_object_id=$1 AND actor_object_id=$2`
	if _, err := store.Exec(q, cuid, uid); err != nil {
		log.Println("go routine error: error while deleting notification, ", err.Error())
	}
}

// blocked users list
func (store *followStore) BlockedUsers(cuid string) ([]Follower, error) {
	var list = []Follower{}
	q := `SELECT id, username, name, avatar, false AS following
		FROM facbook_user WHERE id IN (SELECT user_id FROM profile_blockeduser WHERE owner_id=$1)
		ORDER BY username
	`
	err := store.Select(&list, q, cuid)
	return list, err
}

// block or unblock user
func (store *followStore) BlockUnblock(cuid string, uid string) (BlockUserResponse, error) {
	var status bool
	var resp BlockUserResponse
	q := `SELECT EXISTS(SELECT true FROM profile_blockeduser WHERE owner_id=$1 AND user_id=$2)`
	if err := store.Get(&status, q, cuid, uid); err != nil {
		return resp, err
	}

	if status {
		go store.unblockUser(cuid, uid) // unblock user
		resp.Message = "user unblocked"
		resp.IsBlocked = false
	} else {
		go store.blockUser(cuid, uid) // block user
		resp.Message = "user blocked"
		resp.IsBlocked = true
	}
	return resp, nil
}

// block user
// go routine
func (store *followStore) blockUser(cuid string, uid string) {
	q := `INSERT INTO profile_blockeduser(owner_id, user_id) VALUES($1, $2)`
	if _, err := store.Exec(q, cuid, uid); err != nil {
		log.Println("go routin error: ", err.Error())
	}
}

// unblock user
// go routine
func (store *followStore) unblockUser(cuid string, uid string) {
	q := `DELETE FROM profile_blockeduser WHERE owner_id=$1 AND user_id=$2`
	if _, err := store.Exec(q, cuid, uid); err != nil {
		log.Println("go routin error: ", err.Error())
	}

}
