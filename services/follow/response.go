package follow

// response struct for follow api
type FollowResponse struct {
	IsFollowing           bool   `json:"following" db:"following"`
	Message               string `json:"message"`
	RequestApprovedByThem bool   `json:"requestApprovedByThem"`
	Error                 string `json:"error"`
}

// response struct for block user api
type BlockUserResponse struct {
	Message   string `json:"message"`
	IsBlocked bool   `json:"IsBlocked"`
}
