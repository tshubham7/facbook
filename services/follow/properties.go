package follow

// Follower struct for holding follower list
type Follower struct {
	ID        string `json:"id" db:"id"`
	Name      string `json:"name" db:"name"`
	Username  string `json:"username" db:"username"`
	Avatar    string `json:"avatar" db:"avata"`
	Following bool   `json:"following" db:"following"`
}
