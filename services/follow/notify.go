package follow

import (
	"fmt"
	"log"

	"bitbucket.org/tshubham7/facbook/pushnotification"
	"bitbucket.org/tshubham7/facbook/services/db"
	"bitbucket.org/tshubham7/facbook/services/notification"
	"github.com/jmoiron/sqlx"
)

type Notify interface {
	follow(actoreUserID string, targetUserID string, isRequested bool)
	requestAccept(actoreUserID string, targetUserID string)
}

type notifyStore struct {
	*sqlx.DB
}

func notify() Notify {
	return &notifyStore{db.DBWrite}
}

// create and send notification when the user follow someone
func (store *notifyStore) follow(cuid, uid string, request bool) {
	var u1, u2 string
	var data = notification.NotificationSave{Type: "follow"}
	q := `SELECT (SELECT username FROM facbook_user WHERE id=$1) AS u1,
		(SELECT username FROM facbook_user WHERE id=$2) AS u2
	`
	err := store.QueryRow(q, cuid, uid).Scan(&u1, &u2)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	if request {
		data.Verb = fmt.Sprintf(`≠%s started following me`, u1)
	} else {
		data.Verb = fmt.Sprintf(`≠%s sent me the following request`, u1)
	}
	// send push notification to the device
	pushnotification.PushToUser(uid, data.Verb)
	data.UserID = uid
	data.ActorObID = cuid
	data.ActionObjID = uid
	notification.NewService().Save(data)
}

// send notification to the user for accepting the following request
func (store *notifyStore) requestAccept(cuid, uid string) {
	var username string
	var data = notification.NotificationSave{Type: "info"}
	q := `SELECT username FROM facbook_user WHERE id=$1
	`
	err := store.QueryRow(q, cuid).Scan(&username)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	data.Verb = fmt.Sprintf(`≠%s accepted my following request`, username)
	data.UserID = uid
	data.ActorObID = cuid
	data.ActionObjID = uid
	notification.NewService().Save(data)
	pushnotification.PushToUser(uid, data.Verb) // send push notification to the device
}
